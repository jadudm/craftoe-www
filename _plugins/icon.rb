module Jekyll
  class IconTag < Liquid::Tag

    def initialize(tag_name, text, tokens)
      super
      @text = text
    end

    def render(context)
      tag = @text
      tag = tag.sub(/\s+\Z/, "")
      "<i class='fa fa-#{@text}'></i>"
    end
  end
end

Liquid::Template.register_tag('icon', Jekyll::IconTag)
