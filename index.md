---
title: At a Glance
hideTitle: true
layout: default
listID: "aheINjUy"
---
<p>This site is generated from <a href="https://trello.com/b/aheINjUy">the course Trello board.</a></p>

<p>You may want to see the <a href="https://trello.com/b/aheINjUy/craftoe-sp17/calendar/">course calendar.</a> You can <a href = "https://trello.com/calendar/4e6ffc597c20603d4e03d996/58746176c0ebc4cddfc27b37/dd90aa086071cab4485830536883b452.ics">subscribe to it on your personal calendar</a> if you like.</p>

<p>Submit work to <a href="https://handin.berea.us/">handin.berea.us</a>.</p>

<div class = "row">
  <div id = "boards" name = "boards">

  </div>
</div>


<script>

function getCards (listID) {
  $.getJSON (
    "https://api.trello.com/1/lists/" + listID + "/cards",
    function (list) {
      sorted = list.sort( function (a, b) {
        return a['due'] > b['due'];
        });
      for (card of sorted) {
        // console.log(card);
        tr = $('<tr> </tr>', {
          id: card['id'] + '-row'
          });

        a = $('<a>', {
          href: card['shortUrl'],
          text: card['name']
          });

        cardtitle = $('<td>', {
          width: "60%"
          });
        cardtitle.append(a);
        tr.append(cardtitle);

        if (card['due']) {
          due = $('<td>');
          // console.log("DUE: " + card['due']);
          dsplit = card['due'].split('T')[0];
          // console.log ("SPLIT:" + dsplit);
          due.text(dsplit);
          tr.append(due);
        } else {
          tr.append($('<td> </td>'));
        }

        // console.log("Appending row to: " + listID + "-table");



        $('#' + listID + "-table").append(tr);
      }
    });
}

function grabTrello () {

  $.getJSON (
    "https://api.trello.com/1/boards/{{page.listID}}/lists",
    function (board) {
        for (list of board) {
          // console.log(list);

          therow = $('<div>', {
            class: "row"         
          });

          inner = $('<div>', {
            id: list['id'],
            name: list['id'],
            class: "col-md-8 col-md-offset-1"
            });

          inner.html($('<h3>', { text: list['name']}));
          therow.append(inner);

          tab = $('<table>', {
            id: list['id'] + '-table',
            class: "table table-striped"
          });

          inner.append(tab);
          $('#boards').append(therow);

          getCards (list['id']);

        } // list of board
    });
  };

$(document).ready(grabTrello);
</script>
