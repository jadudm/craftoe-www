---
---

We will add a few more voltage drop questions to our problem set from Tuesday, as well as have a chance to revise questions from Tuesday after Q&A on Tuesday.

So, submit the work on Tuesday (which the TAs will provide instant feedback on), and we'll handle re-submission on Thursday (plus a few questions that will be posted here shortly).
