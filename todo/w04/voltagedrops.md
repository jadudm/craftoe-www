---
---

During the week, you did a lab that introduced the notion of _voltage drops_. We also read a short chapter from the book which had links to four videos where Eeris at Dunwoody College lectured about calculating voltage drops in a circuit.

This weekend, watch those videos again, and take careful notes. I am not going to recreate these lectures in class, and I expect you to learn to carry out these operations. You are, of course, encouraged to ask questions, but there will be no substitute _in class_ for studying these videos, taking notes, and doing the practice problems that I assign. In other words: **you must engage**.

The first five videos recap what we have been doing with Ohm's Law. You may, or may not, find them useful to review. They explore Ohm's Law in series and parallel circuits, and then how to build a table to calculate the voltage and current that flows through different parts of a circuit when components are arranged in series and/or in parallel.

* [Series & Parallel Circuits & Ohm's Law Physics, part 1 - Eeris Fritz](https://www.youtube.com/watch?v=sI8qFPGYXsQ)

* [Series & Parallel Circuits & Ohm's Law Physics, part 2 - Eeris Fritz](https://www.youtube.com/watch?v=ElXIdcVVl3g)

* [Series & Parallel Circuits & Ohm's Law Physics, part 3 - Eeris Fritz](https://www.youtube.com/watch?v=R4aXyYUo1f8)

* [Series & Parallel Circuits & Ohm's Law Physics, part 4 - Eeris Fritz](https://www.youtube.com/watch?v=1cQy7dEVoLM)

* [Series & Parallel Circuits & Ohm's Law Physics, part 5 - Eeris Fritz](https://www.youtube.com/watch?v=tqbU42pFcM8)

You will also find the same material is covered, in textual form, in Chapter 5 of [All About Circuits](http://www.allaboutcircuits.com/textbook/direct-current/chpt-5/what-are-series-and-parallel-circuits/).

## Problems

First, grab the [DC Series Worksheet from our Box.net space](https://berea.box.com/s/jpsepytflj091d2lkk4wu75mf52zhdhl). Please work the following problems and submit them on Tuesday (on paper, please, so you can show your work).

_Note that battery voltages add in series_.

Questions 1, 2, 3, 4, 5, 11, 18, 19, 21, 22, and 23.

## Reminder about Learning

Note that the answers can be found online. If you copy the answers ("cheat," if you will), you will learn nothing. And, when I issue exams, you'll do poorly. The correct solutions to being stuck on these problems are the following:

* Come into the lab and work with a TA.
* Come into the lab and work with a colleague.
* Go to the library and work with a colleague.
* Write me with questions.
* Make an appointment to come and work with me on questions.
* Ask questions in class.

Ultimately, if you do not do the work yourself, you will not learn. And, that is the point. If you try, you'll learn, and you'll do well. If you search for the answers, write them in, and tell yourself that "you'll figure it out later," you'll do poorly.

## Submission

On paper, in class.
