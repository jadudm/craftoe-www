---
---

First, read (skim) the Encyclopedia article on [switches](https://berea.box.com/s/ah66tt0h2yfomhu06d454yak2aj2k1yk) and the [Wikipedia article on LEDs](https://en.wikipedia.org/wiki/Light-emitting_diode). Again, as with all encyclopedic content, skim for highlights and background. We will generally want to understand the whole "single pole single throw" vs. "double pole single throw" thing with switches, and we'll want to know where the positive and negative ends of LEDs are.

We will also want to be able to use these components to do useful things. So, you will also want to revisit the idea of _voltage drops_ with regards to LEDs. I have some problems for you to work on in this regard.

I've got a video about LEDs which, frankly, I think the narrator is boring, but it's good information overall.

<div class = "text-center">
  <iframe width="853" height="480" src="https://www.youtube.com/embed/P3PDLsJQcGI?rel=0" frameborder="0" allowfullscreen></iframe>
</div>

## Problems

Grab the [DC Series Worksheet from our Box.net space](https://berea.box.com/s/jpsepytflj091d2lkk4wu75mf52zhdhl). Please work the following problems and submit them on Tuesday (on paper, please, so you can show your work).

Questions 14, 15, 16, 17, and 20.

Note that the answers can be found online. If you copy the answers ("cheat," if you will), you will learn nothing. And, when I issue exams, you'll do poorly. The correct solutions to being stuck on these problems are the following:

* Come into the lab and work with a TA.
* Come into the lab and work with a colleague.
* Go to the library and work with a colleague.
* Write me with questions.
* Make an appointment to come and work with me on questions.
* Ask questions in class.

Ultimately, if you do not do the work yourself, you will not learn. And, that is the point. If you try, you'll learn, and you'll do well. If you search for the answers, write them in, and tell yourself that "you'll figure it out later," you'll do poorly.

## Submission

On paper, in class.
