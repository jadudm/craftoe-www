---
---

For Thursday, we'd like to start building our Diavolinos (nee Arduinos), as they will let us engage in a large number of other activities. However, there's some work we need to do in preparation.

## Learning to Solder

<div class="row">
  <div class="col-md-6">
    <p>
      The first step is learning to solder. We'll start with reading the well-illustrated <a href="{{site.data.urls.pdfs.solderingiseasy}}">Soldering is Easy</a>. This hits the essentials you need to know clearly and concisely.
    </p>
    <p>
      There will be short quiz regarding this material at the start of class. (The primary focus will be on the practice of soldering.)
    </p>
  </div>

  <div class="col-md-6 text-center">
    <a href="{{site.data.urls.pdfs.solderingiseasy}}"><img src="images/soldering-is-easy.png"></a><br/>
    <small><i>Click me</i></small>.
  </div>
</div>

I would recommend you follow this up with [Adafruit's guide to soldering as well](https://learn.adafruit.com/adafruit-guide-excellent-soldering/tools). This has a short video at the start, and a few pages with a bit more detail on the pages that follow.

## The Diavolino Build

Second, you should read the step-by-step build for the Diavolino. This is the Arduino-clone we're going to build, and you should be comfortable with what the steps are.

Note that I have annotated this version; you should come in with questions if you don't understand my annotations! They are places where we, in our lab, deviate from the manufacturer's instructions... and, they might not be clear/obvious to you at this point.



[The Diavolino Build (PDF)](https://drive.google.com/open?id=0B_8znW3VOlkGTTc5NEFkVEdLSVk&authuser=0)
