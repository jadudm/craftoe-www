---
---

The lab will appear in the book shortly. You'll want to skim it before coming to class. The reading and questions are already a suitable pre-lab.

**UPDATE:** Due to internet interruptions, it was difficult to get the lab updated for you in a timely manner. (The entire book lives on the 'net, which makes working on it, without network, difficult.)

We will do an investigation as a group, briefly, and use it to inform our Q&A regarding voltage drops involving LEDs in the homework. It was expected that you would have questions on this aspect of the homework, and that we would do work in class to help address those questions. *Oddly, however, only one person in 50 even asked any questions about this topic...*
