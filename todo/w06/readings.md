---
---


# For Tuesday

1. Read [Chapter 1](https://berea.box.com/s/yuw3trvkacts5ixy6vukea4zg3bt0oma), [Chapter 2](https://berea.box.com/s/czocc26ywugc7tpjcw4p3dube5o9car1) of Studying Programming.

1. Create an account and [work through the first three (or more!) levels of Help Gidget](http://helpgidget.org/). (If you do the whole training exercise, you will benefit from it. Others have reported it does not take particularly long, and that they have found it very useful.)

1. [Download](https://berea.box.com/s/982pokxs1vtp1m2rmlfmvxdwgrpwoh78) (and unzip somewhere you can find it again... a folder in your Documents, perhaps?) the Arduino programming environment.

# Submission

One of two things:

1. If you have programmed before (perhaps suggesting you have taken at least one of CSC 226, 236, and/or upper-division courses in the CIS program), open the Arduino programming environment and look at ArduBlocks. Explore the blocks available, and document your exploration from two perspectives. First, are there any programming concepts that you think are *missing* that you think would be essential? Second, are there any blocks whose function you *think* you understand, but feel are labeled in an unclear or otherwise confusing manner?

1. If you have not programmed before, please take a screenshot of the highest level you attained in *Help Gidget*. Include this a Word document where you describe (briefly) the concepts you identified in the tutorial, and your best understanding of those concepts (short descriptions). For example, the first few tutorials clearly speak to the idea of *sequencing* one instruction after another. This is, although apparently a simple idea, a critical one in computing.

Of course, you are highly encouraged to make note of questions.

All students are, of course, encouraged to open up ArduBlocks and explore.
