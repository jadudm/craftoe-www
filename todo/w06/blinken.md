---
---

The lab is a [Smartboard file](https://drive.google.com/open?id=0B_8znW3VOlkGRU1XVDVXTjNkVlU). Download that.

Then, use the [Online Viewer](http://express.smarttech.com/) to open the file you downloaded.

The [Easy-To-Install drivers](http://www.ftdichip.com/Drivers/CDM/CDM21214_Setup.exe) are linked early in this sentence.

For schematics, you might use [SchemeIt](http://www.digikey.com/schemeit/). You don't necessarily need an account.
