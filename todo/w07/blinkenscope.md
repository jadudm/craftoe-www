---
---

In this lab, we're going to explore our blinkenlights in detail. 

# DIGITAL EXPLORATION

## Build the Circuit 

First, we're going to build a circuit that has your Arduino, an LED, and a resistor for protection. Anything between 220Ω and 1000Ω will protect the LED.

Go from pin 9, to the LED,  to the resistor, and then to ground. 

## Write the Code

Write a blink program. It should turn on, delay, turn off, delay. Start with a delay of around 10ms.

## Set Up The Scope

Ground your scope CORRECTLY, get a jumper wire, and use the jumper wire so you can attach the probe to one end and plug the other end into the same breadboard block as the high side of the LED.

## Measure the Wave

You should be able to see the waveform on your scope; use your vertical and horizontal zoom and position knobs to center the waveform sensibly. You may need to adjust the trigger level, or use the STOP/RUN function to freeze the waveform.

Once you have done this, first read the divisions on the scope to get a close estimate of how accurate the waveform is. Then, use your cursors to measure from one point where the wave rises to the next (in other words, measure a full cycle). 

Use the screen capture functionality to save a BMP of the screen (with your cursors in place, showing your measurement) to a USB stick.

## Explore Five More Values

At this point, explore five more values. To do this, you'll need to modify your code (first) to use the delay/ms block to drop down to 1ms. Then, switch your **ms** (milisecond) blocks out and use **us**  (microsecond) delay blocks for the 500us, 100us, 10us, and 1us.

* 1ms

* 500us

* 100us

* 10us

* 1us

In each case, use your cursors to measure the full wave width. The thing I'm curious about: how accurate is the delay functionality of the Arduino?

You can, if you wish, take a screen capture of each of these, or you can record the values in a table in your notebook.

# ANALOG EXPLORATION

(I'm not sure if the above exploration will take the full period or not. If it does, then we'll do this next week in class. If the above DIGITAL EXPLORATION goes quickly, then please continue.)

## Write the Code

Write a new piece of code. Get rid of everything, including your loop, and find the "setup" block. In that block, put a single block that writes an ANALOG value to a pin. Make sure you're still using pin 9, and set the value of that block to 128.

## Measure the Wave

You should not need to change your scope setup. 

Use your cursors to measure the wave. Capture the screen with your cursors in place. I'm curious: what is the voltage height (which you should be able to read from the volts/division), and what is the wavelength (which is the rise-to-rise distance).

## Explore More Values

Explore four more values for the analog write to the LED.

* 8 

* 32

* 64

* 256

You do not need to save an image for every one of these measurements; record them in your notebooks.

### Before You Leave

Make sure you and your lab partner have all of the data. It is not acceptable for one person to have the data, and for others to *hope* that it will be shared later.
