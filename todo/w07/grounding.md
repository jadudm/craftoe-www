---
---

For Thursday, please watch this video about scope grounding.

Watch this one closely/fully. It is 24 minutes.

<div class = "text-center">
<iframe width="853" height="480" src="https://www.youtube.com/embed/xaELqAo4kkQ?rel=0" frameborder="0" allowfullscreen></iframe>
</div>

## Submission

1. What is the danger in using a scope with your laptop and USB-powered products?

    1. First, draw the schematic out fully. Explain the parts of the schematic to the best of your ability.

    1. Second, explain the dangerous short that can result from the incorrect use of a scope with a USB powered product.

1. What is the only correct way to use an oscilloscope with an Arduino?
