---
---

For Tuesday, please view the following video:

<div class = "text-center">
<iframe width="853" height="480" src="https://www.youtube.com/embed/CzY2abWCVTY?rel=0" frameborder="0" allowfullscreen></iframe>
</div>

## Reflection Questions

Please answer the following questions in a short document.

1. What is measured on the y-axis?

1. What is measured the x-axis?

1. If you cannot view the top and bottom of a waveform in the scope, what could you do to make sure the entirety of the wave, top-to-bottom, is visible?

1. If you want to measure the wavelength (from peak-to-peak or trough-to-trough), how could you do that? (There's probably more than one way.) I'm not looking for a step-by-step so much as a rough description of what you might do.

1. If you have a waveform that is six divisions tall, and you are set at 0.5V per division, what is the peak-to-trough height, in volts, of this waveform?

1. You have a waveform that starts its rise at 0, and peaks at 2 divisions (horizontally), comes back to zero at 4 divisions, bottoms out at 6 divisions, and comes back to zero at 8 divisions. Each division is 500 microseconds. What is the wavelength (or the duration of one full cycle) of this waveform?
