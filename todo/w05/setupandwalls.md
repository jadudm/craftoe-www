---
---

In class, we will get our Arduino environment set up, and then you will be introduced to our out-of-class-home-wiring walls, which were designed, developed, and assembled by our [Incredible, Amazing TAs](https://www.youtube.com/watch?v=btPJPFnesV4).
