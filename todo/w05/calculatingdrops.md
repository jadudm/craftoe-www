---
---

<style>
.center {
  text-align: center;
}
</style>


These are problems you can use to practice for the quarter quell.

1. Matt sets his power supply to 3V, and drops in a 220Ω resistor. How much current is flowing through his circuit?

1. Matt cranks his power supply up to 30V. This is 10x more voltage. Should the current flowing through his circuit be 10x more as well? Why?

1. At 30V, with a 220Ω resistor, Matt might be in danger of frying his 1/8W resistor. Is it cooked?

1. Matt wants to dissipate 1000W in a circuit at 9V. Obviously, this will have to be a heavy-duty heating element! How much resistance (or, how little resistance) should there be in that heating element?

1. Matt has these tiny little 1/4W resistors. However, some of them are very high resistance... some as high as 10MΩ. What is the maximum voltage that one of Matt's 10MΩ resistors can handle?

1. Matt has a 1500W space heater. He plugs it in to the outlet at 120V. How much resistance is there in his space heater?

1. What kind of current is his space heater consuming?

1. How much does electricity cost in Berea, KY, per kilowatt-hour?

1. A 1500W space heater could also be thought of as a 1.5 *kilowatt* space heater. Just like a 1000Ω resistor is a 1 kiloohm resistor, we tend to discuss power in terms of kilowatts (or *thousands of watts*). How much does it cost Matt to leave this space heater on for 8 hours overnight?

1. Matt decides that he's going to do some fancy home wiring himself. However, he forgets whether he should put his electrical outlets in *series* or in *parallel*. Because he knows how to do the mathematics of series things more easily, he builds a series circuit for his space heaters. So, he now has a circuit that is equivalent to having two resistors in series. Draw this series circuit - a 120V power source and two resistors. (You know the resistance of each of these space heaters from previous questions.)

1. Matt knows that the resistance of the space heaters has not changed, but the power they dissipate *has* changed. This is because he knows that the voltage drop across one heater was 120V. However, in series, the two space heaters will have a smaller voltage drop across each device. How many volts are dropped across each space heater in this circuit?

1. How much power is dissipated by each space heater in this circuit?

1. How much current is consumed, in total, by his series circuit containing two space heaters?

1. Matt runs this circuit for 8 hours. How much does it cost? Is it more, or less, expensive to run the circuit in Berea, KY?

1. Matt, feeling experimental, decides to rewire his house *again*. This time, he thinks his electrical sockets should we wired in *parallel*. (Remember, Matt is not an electrician, and neither are you.) The circuit looks like
    {:.center}
    ![Two in Series](images/two-r-in-parallel.png)

    What is the total resistance of this circuit, using your knowledge of how resistance adds up when they are in parallel (as opposed to when resistors are in series)?

1. What is the total current in this circuit, given a 120V power source?

1. Is the current going to be different if measured in the two locations shown below?

    {:.center}
    ![A1](images/ammeter-pos-1.png)

    {:.center}
    ![A2](images/ammeter-pos-2.png)

1. Is the voltage drop different for R1 than for R2?

    {:.center}
    ![V](images/with-voltmeter.png)

1. What is the voltage drop from the top of R1 to the bottom of R1?

1. What is the power dissipated by the first space heater, R1, given that you know the voltage supply for this circuit, the current in the total circuit, and the voltage drop across this resistor?

1. What is the power dissipated by the second space heater, R2?

1. What is the total power dissipated in this circuit? ([Hint](http://www.allaboutcircuits.com/textbook/direct-current/chpt-5/power-calculations/)).

1. How much does it cost Matt to run his space heaters in this configuration for 8 hours?

1. Matt wants to know whether his space heaters will do the job in his office. His office is 10 x 10', with a 10' ceiling. It has one door, which is roughly 3'x7' in size. He also has a big window... which is roughly 4'x6' in size. The doors and windows have an R-factor (insulation factor) of 1. If we pretend that the building typically holds a temperature of around 65 degrees F, and he wants his office to be 72 degrees F, how many watts of dissipation does the [Chromalox website](https://www.chromalox.com/resources-and-support/calculators/comfort-heater.aspx) think he'll need to heat the space?

1. Matt wonders... his computer tends to run warm when it is doing intense calculations. According to Apple's website, the 27" iMac consumes 63W when idle, and 240W when crunching numbers. This is the same as 215 BTU/h idle and 819 BTU/h when crunching numbers. ([BTUs](https://en.wikipedia.org/wiki/British_thermal_unit) are a measure of heat. You do not need to master BTUs to answer this question.) According to the Chromalox website, how many BTUs does Matt need to keep his office warm? Is his iMac enough?

1. A laptop radiates some power as light (the display), some as radio waves (WiFi), and some is converted to mechanical energy (fan motors)... but the rest is *all* radiated as *heat*. A typical Dell power adapter converts your AC to 19.6V DC and can provide a maximum of 6.5A. When your laptop is working hard, it will draw close to the max. Lets assume that it needs the 19.6V, and draws 5A at maximum load. How much power is your laptop consuming?

1. Given the number of Watts that your laptop radiates, could Matt just leave a Dell laptop running in his office, crunching numbers as hard as possible, and have it heat his office?

The last few problems are, in some ways, just to get you thinking about how this stuff converts into a real-world idea: heating a space. Please don't take this to mean you should master space heating for a mid-midterm.

# Summary

Problems that were explored in this set:

* How to calculate using V=IR
* How to calculate usinv P=IV
* How to combine these equations in common ways.
* How to calculate the total resistance in a circuit, series and parallel.
* How to calculate the total power in a circuit (series or parallel).
* How to calculate voltage drops in a series circuit.
* How to calculate power output from individual components in a circuit, given a known voltage drop.
* How to read a schematic involving power supplies and resistors.
* How to think about power and wattage in terms of real-world activity.

That seems like the extent of the concepts these questions cover. I think they're true to what we've been doing so far.

# Answers

1. 0.013A

1. Yes. The relationship between V=IR is linear and proportional. If V goes up by 10x, then that is the same as multiplying the right-hand side of the equation by 10x. If R is constant, that means that I must be 10x greater.

1. 0.13A is the amount of current. P = IV says that 0.13 * 30 = 3.9A is flowing through the resistor. This is much greater than 0.125W, so the resistor will probably cook.

1. P = IV. 1000 = I * 9, or 1000 / 9 = the current in amps. 111A in this circuit. V = IR, so 9 / 111 = 0.08 ohms of resistance in his heater element.

1. P = IV and V = IR. Therefore, P = I<sup>2</sup>R. 0.125 = I<sup>2</sup> * 10M. I = 0.00011. V = IR then gives us 0.00011 * 10M, or 1100V.

1. P = IV, so I = 1500/120, or 12.5A. V = IR, so R = 120 / 12.5, or 9.6 ohms.

1. From the previous problem, 12.5A.

1. $0.07 per kWh.

1. 8 hours * 1.5kW is 12 kWh. It costs 12 * 7 cents, or $0.84.

1. Diagram omitted.

1. You will want to build a table to calculate the voltage drops; with two equal resistors, it will come out to half the total voltage over each resistor, or 60V over each heater.

1. P = IV, V = IR. I know V, and the resistance of the total circuit is 9.6 * 2, or 19.2 ohms. 120 / 19.2 is 6.25A in the total circuit. Now, multiply that current times each drop, meaning 375W are dissipated by each heater.

1. 6.25A

1. The total power in this circuit is 375W + 375W, or 750W. That is 0.75kW, so the cost for 8 hours is 8 * 0.75 * 0.07, or 42 cents. Given that the total power output of the circuit is half of the single heater, the cost should be half. (Just checking myself.)

1. The total resistance of a parallel circuit is 1/Rtot = 1/R1 + 1/R2. In this case, they're both 9.6 ohms, so the total will be half that, or 4.8 ohms.

1. V = IR, so 120 / 4.8 is 25A.

1. No.

1. No.

1. 120V

1. P = IV, the drop is 120, and the total current is 25A. However, we need to build a table to calculate the current in each branch of the circuit. When we do, we see that R1 and R2 each see half of the total current, meaning each sees 12.5A. Therefore, the power dissipated by R1 is 12.5 * 120V, or 1500W.

1. The second space heater is the same, so it is 1500W.

1. 3000W.

1. Twice as much as just one heater, or $1.68.

1. The site suggests, once all of this data is entered, 92.3W per hour.

1. The website says he needs 315 BTU/hour. When crunching numbers, his iMac will heat the office to more than 72 degrees, if the temperature outside his office is 65 degrees.

1. P = IV. 19.6V * 5A is 98W.

1. Yep. The laptop, running flat out, produces 98W. If we leave that running for an hour, that means 98 Watt-hours of heat have been dumped into the office. To raise/maintain the temperature of his office, he needed 92 Watt-hours, meaning the laptop will do the job.
