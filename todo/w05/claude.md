---
---

For Tuesday, watch this short (8m) video. It is an interview with Claude Steele, a researcher well known for his work on *stereotype threat*.

<div class="text-center">
  <iframe width="853" height="480" src="https://www.youtube.com/embed/failylROnrY?rel=0" frameborder="0" allowfullscreen></iframe>
</div>

## Reflect / Respond

What does this notion of *stereotype threat* make you think about in your own experience? Is it something that resonates with your schooling experience before you came to Berea? At Berea? Within your area of study? How do you think this has manifested, for you?

Take 1000-1500 words to explore this idea (or more, as you like).

## And, Remember...

Practice for the mid-miterm, and **you'll be just fine**. Everything we do in this class you can do, and do well.

## Submission

Upload to Moodle.
