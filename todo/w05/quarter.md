---
---

In class, we'll take the first hour for what the TAs thought might be called the quarter quell (inspired by the Hunger Games). Admittedly, this is a bit grim.

The quarter quell (QQ) will be a set of problems like those you've been practicing in your homework, and have been discussed at length in class. I you get over 80% on the QQ, you'll be exempted from revision, and we will mark it as "full credit." If you get **less** than an 80%, you'll need to revise your work until you get 100% of the problems correct.

## Purpose

This is about thresholded mastery of the material we've covered so far, and making sure you're ready to continue. If you have been doing the work, engaging in class, and making a genuine effort, the QQ should be straight-forward.

If you have been blowing the work off, failing to do the homework, copying answers from colleagues, or generally not engaging in the work of the term, you're going to discover early that you're on track for a final grade in this course that ranges from a C to an F. However, before you dig yourself a deep hole, the QQ will represent a chance to realize the error of that pathway, catch up, and continue forward with confidence.
