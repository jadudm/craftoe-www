---
---

I've found I've been saying the same thing on many of your Braitenburg Vehicle labs. To simplify, I thought I would provide a single page that captures all of these comments, so you can use this as a guide for revision.

## YOUR TASK

Revise your Light Sensor/Braitenberg report and bring the revised version to the final exam. You are welcome to discuss your work in advance of the submission with the TAs and the professor as well as your colleagues. 

## Theory

Your introduction should include the background theory of LDRs and voltage dividers. Specifically, it would be good to talk about how LDRs work, why you need to put them in a voltage divider, what that circuit looks like, and how you then interpret the resulting readings that come from doing an analogRead() on the Arduino. This theoretical background should clearly demonstrate your understanding of the mathematical/theoretical foundations of voltage dividers, and connect it through, as best you can, to the application (a light sensor for a robot).

So, for clarity:

* You want to make sure the theory of voltage dividers is presented.

* You want to present the voltage divider circuit that you used to collect data.

* You want to use the theory to run calculations that demonstrate (say) the expected low, mid, and high-range values for your experimental apparatus.

## Practice

It is common for people to fail to include enough information for the reader to understand **what** you did and **why** you did it. You can use visuals, photos, and circuit diagrams (hand drawn, or drawn with an [online tool][schemeit]) to augment this section (and it is recommended), but you should also use enough words to make sure the reader understands what those visuals mean and how to apply them. 

This is the "methodology" behind your work, in essence. It should be thorough without being tedious. The reader of your report should, without too much difficulty, be able to recreate your work.

Again, for clarity:

* You should present circuits with diagrams, and describe that circuit in text as well.

* You should present code with diagram (or as the code itself, if you programmed in C++ directly), and describe what it does. If you coded in C++, your code should be well-commented, so that the textual description is embedded in the code (as much as possible).

* You should present data in as clear a manner as possible; tables are good, and summary data (averages, etc.) are always welcome. Graphs of data, when possible, are almost always preferred.

* Explain how you did what you did; if you collected data, explain the process by which that data was collected so that someone else could do the same thing.

* Discuss the results of your data, and how it informed your design/process.

## Completeness

You have a theoretical background (LDRs, voltage dividers), data regarding the performance of your sensors, design of circuits and their implementation, background for what a Braitenberg vehicle is (and how you went about planning your build/design), code (and description of its function), and a summary of the findings of your work/overall build. 

Many of you have some of these things, but few labs had *everything*. Often, the things you had were done well, but you want to make 

* Make sure you are writing for a colleague who may be taking electronics, but is not an expert. This suggests your text is complete and clear.

* Make sure you've addressed all aspects of the work you've carried out.

* Make sure you provide diagrams and images that support your work.

## Correctness

Work with your teammantes/colleagues to make sure that what you present is correct. Did you correctly/clearly present theory? Did you include your data and analyze it correctly? Did you explain the process by which you engaged in your data collection and/or design and build? Is your code described correctly in terms of what it does?

The single largest source of "incorrectness" comes from not being clear/thorough enough in your writing. 

## Style

Overall, you are aspiring to an essay, not a bulleted list.

* You should not include the text of the lab in your lab report.

* You should not assume the structure of the report is known to the reader.

* You do not need to write "formally," but phrases like "Well basically," or "We sorta..." don't exactly have a place in your writing.

* Please make sure the names of all authors/contributors are on documents you write.


[schemeit]: http://www.digikey.com/schemeit/