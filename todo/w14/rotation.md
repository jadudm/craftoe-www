---
---

Through the end of the term, we will be working on three of four labs *in rotation*.

* [The Wall Lab](https://docs.google.com/document/d/1w8sUBNWrvg0dgmKXGcTA5EzHMgPdHlPPn587VQCIA0E/edit?usp=sharing) - In groups, you will rotate through the home wiring lab. There will be questions related to this material on the final exam; no writeup is expected at this time.

* [The Braitenberg (Light Sensor) Lab](https://docs.google.com/document/d/1JRc-AY5lQqrntU14XhwMVUXChV9ia3CX7R672Q1kLlI/edit?usp=sharing) - This was the first lab you did in rotation. The writeup is due April 10th, with possible extension to April 10th with an email request.

* [The Button and Knob  Lab](https://docs.google.com/document/d/1tD8-Phbtn63za-TLEmBJnbBWC2wypkxx0K-UyOMmbGc/edit?usp=sharing) - This is the second required lab in the rotation. You will explore using a knob for speed control (or some other feature of your robot) as well as a pull-up resistor with a momentary switch. This lab is due April 18th.

* ... And, one of two more labs (which will appear here shortly). Your final lab writeup is due the 29th of April.

All of your labs should follow the [Scope Lab writeup](http://craftofelectronics.org/todo/w09/scope-writeup.html) as a template. Begin with a study of the pre-readings and preparation material, discuss with your partner how you intend to tackle the lab (design the circuits and experimental process as a pair/trio), proceed with your experimental work, and then integrate your findings into your robotics platform.

You will likely need to use some time outside of lab for your work.

Please ask questions/[make appointments](https://jadudm-berea.youcanbook.me/) as necessary.