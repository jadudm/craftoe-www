---
---

The writeup for lab 1 was described in the book and in class. This first lab is largely diagnostic in nature.

{% include recaptcha filename = "lab01" extension = "docx" %}