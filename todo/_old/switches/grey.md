---
---

For your listening pleasure, the Clapton episode of *The Old Grey Whistle Test*, a BBC television production of the 70's and 80's. 

<div class="text-center">
  <iframe width="640" height="480" src="//www.youtube.com/embed/cGzhWANL1BY?rel=0" frameborder="0" allowfullscreen></iframe>
</div>

## Learning Objectives

None in this class, but it's about time you kids stop listening to that crap music of today and learn what *real* musicians did back in the day.

And, if you're interested in following up further, this playlist seems to have a lot of episodes, and the initial video is actually a history of the show.

<div class="text-center">
  <iframe width="853" height="480" src="//www.youtube.com/embed/le9TKTe1TVM?list=PL8ife9Rz61MusESIGCpKQl7cSghwXlWuN" frameborder="0" allowfullscreen></iframe>
</div>

Oooh! Billy Joel!

<div class="text-center">
  <iframe width="853" height="480" src="//www.youtube.com/embed/A1KXXHM_zj0?list=PL8ife9Rz61MusESIGCpKQl7cSghwXlWuN" frameborder="0" allowfullscreen></iframe>
</div>