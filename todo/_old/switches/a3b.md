---
---

We have two readings:

* [Diodes]({{site.data.urls.pdfs.eocs.diodes}}). It's about time we learn more about them, since we keep using *light-emitting diodes*...

* [Divider Circuits]({{site.data.urls.aac.ch6}}). We're going to lay some theory on top of our understanding of pull-up resistors. To do that, we need to learn more about how to use Ohm's law to calculate values in circuits. Read the first and second sections (or the page linked and the "next" page) of this chapter.

There will be a group sense-making activity and short peer eval based on this reading. In short, if you don't do the reading, I'll ask you and your peers to report that fact.

The sense-making activity will help us 1) summarize what we believe is the essential knowledge from the readings, 2) develop questions from the readings, and 3) practice applying what we read.

## About Reading...

With the readings about circuits, I would recommend you try following along, step-by-step, with the mathematics presented in the chapter. Highly advised. It's OK if you get confused, but it's not OK if you just skim, with no idea what was presented. Active reading requires you to **engage** the text, and do your best to understand what is being presented for you to learn from.

## Learning Objectives

* {{site.data.lo.comp12}}
* {{site.data.lo.theory22}}
