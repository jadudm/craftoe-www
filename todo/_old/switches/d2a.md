---
---

You've been working with resistors, wire, breadboards, and have taken your dive into the build of an Arduino clone (the Diavolio). Today, we're going to make sure all of your Arduinos work, and then work on an exploratory set of circuits that put your pull-up resistor knowledge to work.

## Old Grey Diavolino Whistle  Test

Actually, the phrase is "old grey whistle test," and it referred to the doormen of Tin Pan Alley (a row of music publishing houses in NYC) who, if they kept whistling a tune after hearing a song just once, it was probably going to be a hit, and had passed the "old grey whistle test."

But, here, we're going to use it to mean "testing our Diavolino." I expect this to be a slightly tedious process, because so many things (drivers, software) are going to go wrong.


## ArduBlock Test

First, make sure you can go to the Tools menu and open up ArduBlock.

{% include rimg src="block01.png" %}


### Blink

Assuming I gave a brief intro in class:

{% include rimg src="block02.png" %}

Then, try a few things:

* Change the delay time in either or both locations.

* Change the pin, and try blinking an LED on the board.

* Try blinking both the built-in LED and the LED on the board.

### Variables

What if you want to change the same value in lots of places. Or, better yet, what if you'd rather use a word (like *delayTime*) as opposed to a "magic number" (like *1000*). 

{% include rimg src="block03.png" %}

We'll do more with variables; for now, this is a good use for them: giving names to pins, delays, and other things that we'd like to refer to more than once, and not get confused.

### Printing

It's useful for the Arduino to be able to tell us things.

{% include rimg src="block04.png" %}

In this example, we see how we can have a variable that changes, and every time we go through the loop, the Arduino prints a new value. We can see this by clicking the magnifying glass in the programming environment.