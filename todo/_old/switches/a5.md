---
---

We've explored the pull-up resistor circuit. We've done some reading that probably challenged us. We back-filled some understanding, and now, we're going to reinforce it with some additional viewing and practice.

## Lectures

I find that Eeris Fritz put together an excellent series of short lectures on the topic of parallel and series circuits. I highly recommend you watch and follow along actively. To do that, I would take notes, work the problems she works with her, and come in with questions.

I have included a section of lectures labeled "review." You can review these lectures if you wish, but I believe most of you know this material already. You may find starting at the "calculation" lectures is best.

## Review: Ohm's Law

This first series of three lectures reviews Ohm's Law in series and parallel. I believe most of you are comfortable with this, but I've included them here in case you would like to go back and review.

### Review 1: Ohm's Law

This is a review of Ohm's Law. If you want to start with a refresher on the V=IR (or E=IR) portion of our work, you should begin here.

<div class="text-center">
  <iframe width="853" height="480" src="https://www.youtube.com/embed/sI8qFPGYXsQ?rel=0" frameborder="0" allowfullscreen></iframe>
</div>

### Review 2: Ohms in Series

This lecture lays out the background for what a series circuit is. I think most of you are comfortable with this, but you're welcome to watch and review. 

<div class="text-center">
  <iframe width="853" height="480" src="https://www.youtube.com/embed/ElXIdcVVl3g?rel=0" frameborder="0" allowfullscreen></iframe>
</div>

### Review 3: Ohms in Parallel 

Again, reviewing foundations.

<div class="text-center">
  <iframe width="853" height="480" src="https://www.youtube.com/embed/R4aXyYUo1f8?rel=0" frameborder="0" allowfullscreen></iframe>
</div>

## Calculation: Series

In this series of videos, Eeris Fritz introduces how to use tables to calculate values in these circuits.

### Calculation 1: Calculation in Series

This lecture continues from above, but is the first where we see how to use a table to calculate values.

<div class="text-center">
  <iframe width="853" height="480" src="https://www.youtube.com/embed/1cQy7dEVoLM?rel=0" frameborder="0" allowfullscreen></iframe>
</div>

### Calculation 2: Calculation in Parallel

<div class="text-center">
  <iframe width="853" height="480" src="https://www.youtube.com/embed/tqbU42pFcM8?rel=0" frameborder="0" allowfullscreen></iframe>
</div>

## Learning Objectives

* {{site.data.lo.theory11}}
* {{site.data.lo.theory24}}
* {{site.data.lo.theory31}}

## Submission

Bring to class a set of notes that show the series and parallel problems presented in the videos worked completely. Specifically, the problems presented in *Calculation 1* and *Calculation 2*. I will check these briefly before we begin, so please get them out when you arrive.

We will then proceed to use this new knowledge and attempt to cement it/build upon it through practice with a partner in class. So, your preparedness is important.

{% comment %}
### Lecture 1: Parallel and Series in Combination

Parallel/series combo reduction...

<iframe width="853" height="480" src="https://www.youtube.com/embed/fSuHIQZROmo?rel=0" frameborder="0" allowfullscreen></iframe>
{% endcomment %}