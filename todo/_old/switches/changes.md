---
---

The primary changes:

* **Navbar**. The name "Docs" changed to "Admin." These are administrative documents and links for the course, as well as resources... the syllabus and whatnot.

* **Todo**. A new page, linked from the navbar as "Todo," which includes only the assignments. This is very similar to the front page, but eliminates all the in-class stuff. Potentially a useful resource to some of you. This change was requested by a student, so it is believed to be a good addition.

