---
---

This lab involves building a series of circuits, documenting your work in Fritzing and with a camera. As a report, your challenge is to write up a step-by-step guide on using a pull-up (or pull-down) resistor configuration to read buttons that is *better* for beginners than the guides you read. Your how-to should integrate both the practical aspects of the build and the theory of what a pull-up/pull-down is and why it matters.

## A Floating Wire

First, try reading a floating wire. Plug it into a digital pin, and try reading every 100ms and printing the value. Try fiddling with the wire; do you get different values?

## A Pull-Up

Next, try building a pull-up switch. It will be connected to a digital pin. When the button is pressed, turn on an LED connected to a different digital pin. (For example, the switch/pull-up could be on pin 5, and the LED on pin 9.)

## The Opposite

Now, turn the LED off when the button is pressed.

## A Pull-Down

Now, build the switch circuit with a pull-down. You will probably have to change some of your code to acknowledge the fact that you are now reading the opposite logic levels. (That is, what used to be HIGH is now LOW.)

## Confusion: One of Each

This is tricky:

* Build one pull-up on pin 5; when the button is pressed on the pull-up, turn on the LED on pin 6.

* Build one pull-down on pin 8; when the button is pressed on the pull-down, turn off the LED on pin 9.

## CHALLENGE: Switches and Variables

Here's a trick: can you turn an LED on when one button is pressed, and off when another button is pressed? That is, press the first button, and the LED stays on. Press the other, and it stays off...

This stretches your thinking about how you would program this; it is pushing you faster in terms of thinking about programming than I would normally, but perhaps you'll enjoy/rise to the challenge? Who knows!

**HINT**: You'll need a variable whose value you change, and an **if** block that decides what to do based on that variable.

## Submission

The report has its [own submission page](a3.html).