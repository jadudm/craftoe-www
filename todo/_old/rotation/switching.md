---
title: "Switching Loads with a Transistor"
---
This lab has two parts; you should be able to complete each part in a single period.

## The Transistor

Your Arduino can handle 5V inputs and outputs, with an upper limit of roughly 40mA; some Arduinos are actually 3.3V devices, meaning they can manage even less electrical potential. However, many devices in the world run at higher voltages and currents... does this mean our Arduino is useless?

Obviously not! It just means we need to learn how to use a low-voltage device to control higher-voltage devices. A common use with an Arduino is to use a transistor to control things like a motor, a bank of LEDs (that run at a higher voltage/current than you can switch directly), or any of a number of other devices.

### Learning

At the least, you should definitely read:

* [Sparkfun: Transistors](https://learn.sparkfun.com/tutorials/transistors)
    Note that the transistors reading from Sparkfun goes into amplifier design, which is not pertinent to this particular laboratory.


* [Sparkfun: Motors](https://learn.sparkfun.com/tutorials/motors-and-selecting-the-right-one?_ga=1.202601555.33770683.1409584091)

* [TIP120 Datasheet](http://www.onsemi.com/pub/Collateral/TIP120-D.PDF)
    In particular, what is the minimum and maximum voltage and current this transistor can handle?

For greater detail, you should probably skim/review:

* [Encyclopedia: Transistors]({{site.data.urls.eocs.transistors}})

* [Encyclopedia: Motors]({{site.data.urls.eocs.motors}})

{% comment %}
### Prep Questions

Before you come to lab, you will want to answer the following questions so as to prepare you for the work you will engage in.

1. Look at the datasheet for the TIP120.
{% endcomment %}

### Pieces-Parts

For this lab, you'll need the following bits:

* A TIP120 NPN-transistor
* A benchtop power supply
* Wire
* Arduino
* DC Motor

### The Circuit

{% include rimg src="transistor-01.png" %}

### The Code

When you set the pin connected to the transistor to HIGH, you will bias the base of the transistor, allowing current to flow from the collector to the emitter. This "closes" the switch, making the motor run.

When you set the pin to LOW, it will turn off the motor.

Write a simple program that turns the motor on for three seconds, delays for three seconds, and then repeats.

### Reflection and Assessment

First, capture a picture of your work, and draw a clean schematic of this circuit using Fritzing; make it neat. Include your code, and explain anything interesting in both your circuit and/or code.

1. What role does the diode play in this circuit? Hint: It is commonly referred to as a *flyback diode*.

1. Why did we put a 1K resistor between the Arduino and the transistor? What does it do? Your answer should leverage your knowledge of the basic laws of electrical circuits and the role resistors play in those circuits; feel free to include a calculation, and refer back to the datasheet for the TIP 120.

1. What voltage must be present on the base in order to "flip" the transistor? Must it be 5V? What does the datasheet say? If it is something other than 5V, could we use a variable resistor as part of a voltage divider to trigger this circuit as well? What would it look like? (Sketch and explain.)

This work may be submitted in conjunction with a partner; you will both receive the same assessment, and any revisions will need to be carried out separately.

## The Relay

The relay is a physical switch that is controlled by an electrical signal. This means that it is capable of switching much greater voltages and currents than a transistor; it is also physically slower. The ratings for our relays are on the physical device... you should immediately see that it is capable of switching far greater voltages/currents than the TIP120.

### Learning

This tutorial is reasonably approachable:

* [Relays](http://www.electronics-tutorials.ws/io/io_5.html)

And, for more detail:

* [Encyclopedia: Relays]({{site.data.urls.eocs.relays}})

### Pieces-Parts

For this lab, you'll need the following bits:

* A relay module
* A benchtop power supply
* Wire
* Arduino
* DC Motor

### The Circuit

{% include rimg src="relay-01.png" %}

### The Code

When you set the pin connected to the relay to HIGH, you will cause the relay to close its contacts, thus allowing current to flow through to the motor.

When you set the pin to LOW, it will turn off the motor.

Write a simple program that turns the motor on for three seconds, delays for three seconds, and then repeats.

**HINT**: I would recommend adding an LED on another pin, and turn it on and off at the same time, so that you can tell when you are turning things on, and when you are turning things off.

### Reflection and Assessment

First, capture a picture of your work, and draw a clean schematic of this circuit using Fritzing; make it neat. Include your code, and explain anything interesting in both your circuit and/or code. Then, reflect on and discuss the following questions in a short document.

1. What would happen if you switched the power input to the relay from NO to NC? Your LED should help you explain this.

1. Could you use your relay to control, say, the power to the hot water heater in the lab?

1. You've decided to automate your home, because you now know how to turn things on and off. Would you use the relay or the TIP120 to control a 60W CFL light bulb (which draws 13W)? Why?

1. You've decided to automate your home, because you're an Arduino-super-electronicist. What safety precautions would you take in developing your home automation circuitry?

This work may be submitted in conjunction with a partner; you will both receive the same assessment, and any revisions will need to be carried out separately.

## Regarding Assessment

Excellent reporting will present clear diagrams and sketches, full paragraphs and sentences, and generally be easy to read and correct. Thorough, complete reporting that demonstrates learning is the goal.

Proficient reporting misses on any one of these dimensions. Non-proficient reporting is incorrect, sloppy, and demonstrates an overall lack of commitment to clarity and correctness.
