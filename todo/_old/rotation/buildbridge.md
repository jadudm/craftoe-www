---
title: "Building the H-Bridge"
---

This lab has you building a complex circuit and programming it to control a motor. You'll want to work together and pay close attention to the work you are doing, checking your work step-by-step.


## The H-Bridge

The h-bridge is a common circuit for controlling small motors. It is most often encountered as a solid-state integrated circuit, sometimes packaged in multiples so that you can control two or more DC motors. They will vary in the voltage and current they can handle, allowing you to control small, toy DC motors all the way up to large industrial motors. 

In short, this is a foundational circuit of DC motor control. (AC motors are very, very different... and make sure you never confuse one for another. Read the panel, and make sure you are dealing with a motor rated with a voltage, and not VAC. VAC means "Voltage Alternating Current", and you could do some real harm to yourself, the equipment, and others if you mix these two up.)

This lab is inspired in part by work done at the [Robot Room](http://www.robotroom.com/BipolarHBridge.html).

### Learning

At the least, you should definitely read:

* [Sparkfun: Transistors](https://learn.sparkfun.com/tutorials/transistors)
    Note that the transistors reading from Sparkfun goes into amplifier design, which is not pertinent to this particular laboratory.
    
* [Sparkfun: Motors](https://learn.sparkfun.com/tutorials/motors-and-selecting-the-right-one?_ga=1.202601555.33770683.1409584091)

* [H-Bridge Secrets](http://www.modularcircuits.com/blog/articles/h-bridge-secrets/h-bridges-the-basics/)

* [2N3904 Datasheet](http://www.ece.rice.edu/~jdw/data_sheets/2N3904.pdf)
    In particular, what is the minimum and maximum voltage and current this transistor can handle?

For greater detail, you should probably skim/review:

* [Encyclopedia: Transistors]({{site.data.urls.eocs.transistors}})

* [Encyclopedia: Motors]({{site.data.urls.eocs.motors}})

### Pieces-Parts

For this lab, you'll need the following bits:

* 2N3904 and 2N3904 transistors (don't mix them up!)
* 1N4001 or 1N4004 diodes
* A benchtop power supply
* Wire
* Arduino
* DC Motor

### The Circuit

For this circuit, I am going to challenge you to read the schematic, and build the breadboard circuit from the schematic. It is a critical skill, and you will practice it here.

{% include rimg src="hbridgebuild.png" %}

Important things to note:

* Q1 and Q3 are NPN transistors (2N3904s).

* Q2 and Q4 are PNP transistors (2N3906s).

* The resistors are to protect the control pins on the transistors from receiving too much current. 

* The diodes prevent current from flowing back through the transistors the wrong way when the motor stops; they're flyback protection, as in other circuits we've built with DC motors.

### The Code

To control your motor, you need to carefully apply voltage to combinations of pins. *Please do not become confused between resistor numbers and digital pin numbers in the schematic!* Read carefully, and work with your partner to wire things up correctly.

#### Forward

To make the motor go forward, you must set 

* D3 - HIGH
* D4 - HIGH
* D5 - LOW
* D6 - LOW

#### Coast/Off

To stop your motor, you must set

* D3 - HIGH
* D4 - LOW
* D5 - HIGH
* D6 - LOW

#### Reverse

To run your motor in reverse, you must:

* D3 - LOW
* D4 - LOW
* D5 - HIGH
* D6 - HIGH

#### Making It Go

To see your motor work in all possible directions, I recommend you do the following:

* Create a procedure called *forward*. It should set the pins as described above for going forward.

* Create a procedure called *coast*. It should set the pins as described for coasting.

* Create a procedure called *reverse*. It should set the pins as described for going in reverse.

In your loop, then do the following:

* Use the procedure *forward*.
* Delay for 5 seconds.
* Use the procedure *coast*.
* Delay for 1 second.
* Use the procedure *reverse*.
* Delay for 5 seconds.
* Use the procedure *coast* again.
* Delay for 1 second.

This will alternate the motor going forward and backward. I suggest you put a bit of tape on the motor to see it running.

This documentation from App Inventor, another block-like language, [might help you understand procedures better](http://appinventor.mit.edu/explore/ai2/support/concepts/procedures.html).

#### WARNING

**Never** set pin D6 HIGH and D5 LOW at the same time! You'll short circuit the power supply.

**Never** set pin D4 to HIGH and pin D3 LOW at the same time! You'll short circuit the power supply.

Both of these situations **could result in blue smoke being released** somewhere, possibly with danger to you (and certainly with danger to your equipment).


### Reflection and Assessment

First, capture a picture of your work, and draw a clean breadboard of this circuit using Fritzing; make it neat. Then, do a layout of this circuit for a PCB; this would be an excellent circuit for us to cut and make permanent if we wanted to use it in (say) a small robotics project.

Include your code, and explain anything interesting in both your circuit and/or code. 

1. Are you able to make your h-bridge PCB a single-sided board? If not, what seems to be the limitation in this circuit?

1. What is a *procedure*? We wrote a few in this lab. What does this programming construct seem to do, in your opinion?

1. Explain why the motor goes forward when you set pin D3 and D4 to HIGH? In other words, explain how the h-bridge works when we positively bias the bases of transistors Q3 and Q4.  

1. Are h-bridges efficient? That is, do we lose a lot of power using a circuit like this, or is this a good way to control a DC motor?

This work may be submitted in conjunction with a partner; you will both receive the same assessment, and any revisions will need to be carried out separately.


## Regarding Assessment

Excellent reporting will present clear diagrams and sketches, full paragraphs and sentences, and generally be easy to read and correct. Thorough, complete reporting that demonstrates learning is the goal.

Proficient reporting misses on any one of these dimensions. Non-proficient reporting is incorrect, sloppy, and demonstrates an overall lack of commitment to clarity and correctness.

