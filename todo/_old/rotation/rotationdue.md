---
---

Each laboratory has a series of questions at the end. My ask is that you:

1. Provide a brief overview of your process. Document your build with some pictures. This is a short "what did you do?" section.

2. Address the reflection questions at the end of the lab

3. Finally, reflect on what you took away from this lab. Highlight any particular challenges you encountered, and any significant points of victory!

When you are done, submit your writeup to Moodle.

The first lab may be written up with a partner. The subsequent labs are to be written up individually, so that I can better assess what you took away from the laboratory experience as individuals.
