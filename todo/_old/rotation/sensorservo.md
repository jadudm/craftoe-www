---
title: "A Sensor-Controlled Servo"
---

This laboratory has you exploring the control of a servo with one or more sensors.

## Servo and the Sensor

Servos are another kind of DC motor, but give us the ability to position them. Our servos can be positioned between 0 and 180 degrees, but there are 360-degree and continuous rotation servos as well. They are characterized as being low-torque, but expensive servos can give you precision and power together.

In this lab, you will build a mini solar tracker.

### Learning

At the least, you should definitely read:

* [Seattle Robotics: Servos]()

* [The Adafruit Servo Tutorial](https://learn.adafruit.com/adafruit-arduino-lesson-14-servo-motors/overview)
    We will be following this for the start of the lab.
    
* [The Mini Solar Tracker Instructable](http://www.instructables.com/id/Arduino-Solar-Tracker/)

For greater detail, you should probably skim/review:

* [Encyclopedia: Servos]({{site.data.urls.eocs.servos}})

* [Light Tracking Tutorial](http://stigern.net/blog/?p=103)
    This is another look at a two-sensor tracker; it might serve as inspiration for a starting point.

### Pieces-Parts

For this lab, you'll need the following bits:

* One (or two) micro servos
* A potentiometer
* Wire
* Arduino
* Two LDRs (and matching resistors) or two analog light sensors.
* Hot Glue

Hot glue is electronics safe, and can help you "permanently" build a small light tracker without having to actually make anything "permanent." Bits of card stock, cardboard, and tape might work, too. (In other words, you're making something that has to last a few days, not a few years...)

### Before You Begin

First, follow the Adafruit tutorial, and get comfortable using the servo. You'll have to convert the code you find there into ArduBlocks code.

### The Circuit

You're going to have to design your circuit for this one. It will have two sensors (that you read analog values from) and one servo (which you connect and control as described in the Adafruit tutorial above). To do left/right control, you need two sensors and one servo; to do left/right and up/down, you'll need four sensors and two servos.

Draw your circuit out, neatly, in Fritzing before you begin. Make sure you are confident about your design before risking components.

A few notes:

1. Study how the Instructable does it. This isn't the best solution, but notice that they're not too worried about it being "pretty."

1. If you want, use only two sensors, not four. If you use two, then you'll only be controlling left/right. If you use four, you can do up/down as well. I recommend starting with two and be successful that way first.

1. I've printed [this](http://www.thingiverse.com/thing:107957). If you want to use this as your base, you are more than welcome.

### The Code

You have several code examples from the various tutorials; can you translate them to Ardublocks? 

Ultimately, you want to make it so that if one light sensor sees brighter light than the other, you want to turn towards the sensor that sees the bright light. You should probably allow for some error... for example, if one sees 500 and the other sees 480, you might consider those "the same."

You might also find some of [this page from last term of use](http://craftofelectronics.org/archive/fall-2014/todo/techfoundations/d5.html). Or, not.

### Reflection and Assessment

First, capture a picture of your work, and draw a clean schematic of this circuit using Fritzing; make it neat.

Include your code, and explain anything interesting in both your circuit and/or code. 

1. To track the sun, do you actually need sensors? What information do you need to know where the sun is at any given time of day?

1. Which sensors did you choose for this lab, and why?

1. Did your code work well, or do you think it could use improvement? If you felt it could do a better job of controlling your tracker, what would make it better do you think, and why?

1. Look at [this set of resources collected by colleagues of yours](https://docs.google.com/document/d/1KZ243uTzVrdhvJ4HY7ygyuJd8lLdLQB0hUfuC4pgaXw/edit) last term. What are three things that you believe would be critical if you were going to actually build a sun-tracking solar panel for power generation use? Explain.

1. From the same set of resources, what skills seem to be necessary to build a solar panel array? What skills do you feel comfortable with, and what skills would you need to develop further in order to do so?


This work may be submitted in conjunction with a partner; you will both receive the same assessment, and any revisions will need to be carried out separately.


## Regarding Assessment

Excellent reporting will present clear diagrams and sketches, full paragraphs and sentences, and generally be easy to read and correct. Thorough, complete reporting that demonstrates learning is the goal.

Proficient reporting misses on any one of these dimensions. Non-proficient reporting is incorrect, sloppy, and demonstrates an overall lack of commitment to clarity and correctness.

