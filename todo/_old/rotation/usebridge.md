---
title: "Using an IC H-Bridge"
---

This lab has you building a circuit and programming it to control a motor. You'll want to work together and pay close attention to the work you are doing, checking your work step-by-step.

## The Integrated Circuit H-Bridge

The h-bridge is a common circuit for controlling small motors. It is most often encountered as a solid-state integrated circuit, sometimes packaged in multiples so that you can control two or more DC motors.  In this lab, you will use one of those integrated circuit h-bridges to control two motors. This lab is related to another lab in this sequence, where you build an h-bridge using transistors and diodes to control a single motor.

### Learning

At the least, you should definitely read:

* [Sparkfun: Motors](https://learn.sparkfun.com/tutorials/motors-and-selecting-the-right-one?_ga=1.202601555.33770683.1409584091)

* [H-Bridge Secrets](http://www.modularcircuits.com/blog/articles/h-bridge-secrets/h-bridges-the-basics/)

* Oscilloscope prep...

* [2N3904 Datasheet](https://octopart.com/search?q=SN754410)
    In particular, what is the minimum and maximum voltage and current this transistor can handle?

For greater detail, you should probably skim/review:

* [Encyclopedia: Motors]({{site.data.urls.eocs.motors}})

### Pieces-Parts

For this lab, you'll need the following bits:

* SN754410 H-Bridge
* A benchtop power supply
* Wire
* Arduino
* DC Motor

### The Circuit

For this circuit, I am going to challenge you to work from a partial schematic, and propose a good breadboard Fritzing diagram before you begin your build. In other words, you need to use the information presented below to put together a sensible circuit in Fritzing before you begin your build.

{% include rimg src="hbridge-layout.jpg" %}

**HINT**: "logic" means "a digital pin on the Arduino". VCC1 is a 5V supply from your Arduino, while VCC2 is the power supply from the benchtop power supply (probably 6V).

You will end up with two motors, one connected to the pins 1Y and 2Y, and another motor connected to 3Y and 4Y.

I also recommend you put a 10uF capacitor between power and ground on each motor. This is called a *decoupling capacitor*, and will help make sure everything doesn't reset every time you turn on a motor.

### The Code

To control your motor, you need to carefully apply voltage to combinations of pins. *Please do not become confused between resistor numbers and digital pin numbers in the schematic!* Read carefully, and work with your partner to wire things up correctly.

#### Forward

To make the motor go forward, you must look at the table in the picture above. You will need to write code that sets pins EN, 1A, and 2A to either HIGH or LOW (H or L) to make the motor behave as described. So, first choose which digital pins will be used for EN, 1A, and 2A. (You should have already done this in your diagram!) 

You might create a *procedure* that is called *forwardOne*. It could set the EN pin to HIGH, the 1A pin to LOW, and the 2A pin to HIGH. This will make it "turn right." This depends on how you wired the motor, so... it's a bit hard to say which way it is going. 


#### Coast/Off

Follow the pin configuration for one of the three "stop" conditions described in the image above. Create a procedure for this called *stopOne*.

#### Reverse

Create a procedure called *reverseOne* that turns the opposite of the direction you chose for *forwardOne*. 

#### The Other Motor!

Now, create procedures called *forwardTwo*, *stopTwo*, and *reverseTwo*. It should control the *other* motor, which you will connect to pins 3Y and 4Y. You will be controlling the digital pins 3A and 4A in these procedures.

#### Making It Go

To see your motor work in all possible directions, I recommend you do the following
in your loop:

* Use the procedure *forwardOne*.
* Delay for 5 seconds.
* Use the procedure *stopOne*.
* Delay for 1 second.
* Use the procedure *reverseOne*.
* Delay for 5 seconds.
* Use the procedure *stopOne* again.
* Delay for 1 second.
* Use the procedure *forwardTwo*.
* Delay for 5 seconds.
* Use the procedure *stopTwo*.
* Delay for 1 second.
* Use the procedure *reverseTwo*.
* Delay for 5 seconds.
* Use the procedure *stopTwo* again.
* Delay for 1 second.

This will alternate each motor going forward and backward. I suggest you put a bit of tape on the motors to see them running.

As a twist, create another program (do a "Save As" and edit the one above) that turns on both motors at the same time, stops them at the same time, and so on, so we see both running together.

This documentation from App Inventor, another block-like language, [might help you understand procedures better](http://appinventor.mit.edu/explore/ai2/support/concepts/procedures.html).

#### WARNING

**Never** set pin 1A and 2A (or 3A and 4A) to HIGH at the same time! You could damage the h-bridge.

### Reflection and Assessment

First, capture a picture of your work, and draw a clean schematic of this circuit using Fritzing; make it neat. Then, do a layout of this circuit for a PCB; this would be an excellent circuit for us to cut and make permanent if we wanted to use it in (say) a small robotics project.

Include your code, and explain anything interesting in both your circuit and/or code. 

1. Are you able to make your h-bridge PCB a single-sided board? 

1. Look up *motor driver* on Sparkfun. How many products can you find that are for controlling motors? What kind of price range can you find, and what seems to be the critical differences between them? (This question will take some time and research.)

1. You are developing a small robot to take part in a competition; you can either purchase a motor driver, or you can make one using the SN754410. What factors might lead you to choose your own circuit built with the SN754410? What might cause you to consider using a purchased component?

1. First, modify your circuit: make sure that you have 1A connected to pin 3, and 2A connected to pin 5. Go back to your code. Then, change the procedures *forwardOne* and *reverseOne* as follows:

    1. Make sure you have the correct pin numbers; you need to be using pins 3 and 5 for 1A and 2A. (EN can be left alone.) Now, instead of setting pins 3 and 5 as HIGH or LOW, change them so that pin 3 (only) uses analog values, and set it to either 127 (instead of HIGH) or 0 (instead of LOW). 
    
    1. Experiment with changing the value 127. It can be any number between 0 and 255. What happens when it is low? What happens when it is high?
    
    1. Experiment with doing the same thing as in the previous step, but change pin 5 from LOW to HIGH. That is, see what happens when you are setting analog values on pin 3 while pin 5 is LOW, and what happens when you do the same thing with pin 5 being HIGH.
    
    1. Remove the motor, and get out an oscilloscope. Put the probe on pin 3 of the Arduino and the ground of the probe on GND. What do you see? Try changing the values again from low values (around 0) to medium values to high values. What is going on? Why might this make the motor go faster or slower?
    
This work may be submitted in conjunction with a partner; you will both receive the same assessment, and any revisions will need to be carried out separately.

#### Acks

This lab is inspired in part by work done at the [ITP](https://itp.nyu.edu/physcomp/labs/motors-and-transistors/dc-motor-control-using-an-h-bridge/).


## Regarding Assessment

Excellent reporting will present clear diagrams and sketches, full paragraphs and sentences, and generally be easy to read and correct. Thorough, complete reporting that demonstrates learning is the goal.

Proficient reporting misses on any one of these dimensions. Non-proficient reporting is incorrect, sloppy, and demonstrates an overall lack of commitment to clarity and correctness.

