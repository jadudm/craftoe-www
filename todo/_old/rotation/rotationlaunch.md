---
---

At the start of each rotation, you should look up in Moodle who your partner is, and which lab you are doing. You should then read the lab and do the prepatory reading (and answer the prep questions, submitting them to Moodle) before coming to lab.

## Switching Loads

This laboratory is about using both a transistor and a relay to switch a high-voltage load with a low-voltage signal.

* [Switching Loads with Transistors and Relays](switching.html)

## Building The H-Bridge

This laboratory is one of two that investigates DC motors and their control.

The h-bridge is a circuit built from transistors that can be used to control a DC motor. Often, an h-bridge is purchased as a single chip; however, it can be constructed using individual components (transistors).

You will study and build an h-bridge, and then use it to control a small DC motor in this lab.

* [Building the H-Bridge from Components](buildbridge.html)

## Using the H-Bridge

This laboratory is one of two that investigates DC motors and their control.

While it is possible to build an h-bridge from individual components, this is error-prone (but, cheap and important to know how to do, because you won't always have the standalone component). In this lab, you'll use an IC (integrated circuit) version of an h-bridge to control two motors. This could serve (for example) as the foundation for a small robot.

* [Using an Integrated H-Bridge](usebridge.html)

## Sensors and Servos

In this lab, you'll either use a sensor you're familiar with (light, temperature) or a new sensor (distance, motion) to control a servo. It introduces servos and, optionally, a new sensor.

* [Sensing and Servo Control](sensorservo.html)
