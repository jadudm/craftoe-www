---
---

Due to the confusion of snow days, you got off lightly these last few days. Things were not updated because of the confusion as to when, or if, we might have class.

Please come to class today from 11:30 AM to 12:50 PM.

Cheers,
Matt
