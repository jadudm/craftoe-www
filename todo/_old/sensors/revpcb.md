---
---

### Work Summary

We had a Fritzing [diagram due]({{site.base}}/todo/switches/a4.html) on Tuesday. This is a [place to submit your revisions]({{site.data.urls.submission.switchesa5}}) to that diagram. Remember, our goal is a clean circuit (small) that we can cut on the PCB mill.

## About Revisions

Revisions are due within a week of feedback being received. In the case of the Fritzing diagrams, I gave feedback on many PCBs in-class. As a result, I consider **all** of you to have received feedback. You should have enough information to clean up your breadboard, schematic, and PCB views of your circuits for re-submission.
