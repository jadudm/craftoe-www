---
---

<div class="text-center">
 {% include rimg src="bat-signal.png" %}
</div>

THIS JUST IN:

<div class="well">
  <p>Matt,</p>
  <p>I have a visually impaired student in my Science for Teachers class.  One of the significant projects we're working on is a sundial—which she will obviously have trouble engaging with.  I am wondering if one might make a light activated buzzer that would detect the presence of a shadow (indicating the time) so that we could adapt the project for her.  I did five minutes of internet research and found this source:</p>

<p>  <a href="http://www.electronicshub.org/light-activated-switch-circuit/">http://www.electronicshub.org/light-activated-switch-circuit/</a></p>

  <p>So the questions are a) do you think something like this might be feasible, and b) might there be someone in the Danforth building who would be willing to build such a device?  It would need to be completed within a week or so.</p>
  
</div>

WELL, BAT-BEREANS, WHAT NOW?