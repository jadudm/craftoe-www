---
---

You should have sprinted on your portion of the sensor design over the weekend. We have one week to bring this together at this point, which will require teamwork (within teams and across teams). It isn't *too much*, but it does require us to *focus* and *get stuff done now*.

# For Tuesday

You all need to sprint as far as possible on each section of the project.

## Breadboard Prototype

It would be ideal if we came into class with a breadboard prototype built or ready to be built. This suggests that you came in Sunday or Monday evening and did some work. 

At the least, we need a good circuit design.

## Fritzing Sketch

In conjunction with the breadboarding team, a Fritzing sketch (on the virtual breadboard) would take us a long way to having a circuit we can cut on the mill. As soon as we have a circuit ready to go, we'll cut one.

## Enclosure

We need a working CADD sketch that we can use to print or lasercut an enclosure. The sooner we can get a prototype knocked out, the better. If we're lasering, cardboard is cheap and fast. If we're 3D printing, it will be slower.

## Code

We need a thorough flowchart at the least. We need to know what we're going to read, where we're going to store it, how we're going to use the values we read and store, and what the output will be. 

# On Tuesday

We'll come together in our respective groups---code, enclosure, sketch, circuit prototype---and compare notes. We want to make sure that we don't have any radical differences. At this point, it is *really a good idea* to compare notes and make sure you have a good design for your part of the project.

Then, we'll break back out into mixed teams, and bring ideas together. 

In a perfect world, we'd have something ready to print/cut/mill on Tuesday.

