---
---

On Thursday, we really need to be printing/cutting/milling our first prototype. If we're going to have something built by next Tuesday, we have to be prototyping *now*.

I'll talk to Dr. Saderholm about bringing a sundial over for us to do tests on.