---
---

We had a lot of interruptions due to weather, Labor Day, and the like. This midterm checkpoint helps us:

1. Review what we've done, and

2. Get back in the groove, so that

3. We can rock out for the second half of the term.

In all cases, you might be ready to answer the question directly, or you might know the theory, and need to do some research to answer.

## Resistance - Basics

Q1. Matt has built a circuit on a breadboard using solid core, 22 gauge wire. (That's the stuff we have in the lab.) How much resistance do we assume the wire adds to the circuit?

Q2. Matt has 1000 feet of 22 gauge wire on a spool; what is the resistance of the entire spool, if he measures the resistance (using a multimeter) of the entire length of wire?

Q3. Matt also has a 100 foot spool of romex cabling, the stuff typically used in homes. What wire gauge is typically used in romex?

Q4. What is the resistance of Matt's 100 feet of romex?

Q5. If Matt cuts 100' of the 22-gauge wire from a spool, will the romex have higher resistance, or the 22-gauge wire? Why?

Q6. What are the common resistor values between 200 ohms and 600 ohms?

Q7. If we have a resistor that has 10% error, and it has a rated value of 1.2K, what are the minimum and maximum possible values it might actually read when we put a multimeter on it?

Q8. If the same resistor is rated at 1% error, what are the minimum and maximum values we might see on a multimeter?

Q9. If we are using a 5V power source, a 10K resistor, and we want to know how much current we might get out of the circuit, what are the minimum and maximum values for current that we can expect if the resistor is rated at 5% error?

Q10. A voltage divider has two resistors in series. An *equivalent circuit* would be one where we represent the resistance of the entire circuit as just one resistor. If I build a voltage divider from a 130 ohm and 180 ohm resistor, what is the equivalent resistance of this circuit?

## Soldering and Fabrication

Q11. The part of the soldering apparatus we hold is commonly referred to as the...

Q12. Every large lab in the building has a toolbox equivalent to those that you work with in TAD 265. You've been asked to repair a connection in a drill press; wire lugs (the lug is that round bit, so you can screw it down to a connector) like those depicted below are used on all of the wires in the motor housing. After removing the old lug, and crimping a new lug on, you are going to solder it.

    A. First, what are some reasons for why it is a good idea to solder the lug to the wire?

    B. Second, based on the image below, what would you argue is the best place/way to solder the lug to the wire?

<img src="images/solderlug.png">

Q13. Matt has decided that the new Arduino should have surface mount components instead of through-hole components. What size surface mount resistors do you recommend he use on the board?

Q14. We currently use an ATmega328 (the processor on our Arduino) in a 28-pin DIP configuration.

    A. What does DIP stand for?

    B. Matt decides we should use the same chip, but in QFP configuration instead. How would you go about soldering the QFP chip to the board?

    C. Matt has lost his mind, and things a BGA processor is the way to go. Could you solder a BGA processor to the board with your iron? What tool(s) would you need to do this?

## Practicalities and Fritzing

Q15. You're running a data collection experiment over the weekend. What are some possible ways that things could go wrong with your current sensor design/build during that time? Explain each briefly. (Limit yourself to a half-dozen concerns or so... no doubt, the number of possible failure modes are nearly infinite.)

*Matt realizes that he cannot run the same experiment in his own home because his nearly-two-year-old will tear a breadboard-based sensor apart, and his nearly-six-year-old will reuse the parts (without asking) to make a robot.*

Q16. Matt decides the solution is to make an *Arduino shield*. This will plug into our existing shield stack, and include all of the sensors directly on the breadboard. He managed to draw the circuit on the breadboard, but ran out of time and didn't manage to actually route the PCB. Please clean up his schematic and, to the best of your ability, create a neat, single-sided PCB in Fritzing. [A starter file has been provided for you](images/datacollector-shield.fzz), created using Fritzing version 0.9.1b in Linux. (If you have problems with it, please contact Matt.)

## Kirchhoff's Laws, Ohm's Laws, In-laws and Bylaws

These questions ask you to use V=IR, P=IV, and other theoretical constructs we have learned to answer practical questions about powering DC devices.

Q17. In a circuit with a battery and two resistors, how much electrical potential remains after going through both resistors?

Q18. Batteries are rated in amp-hours or, for smaller batteries, milliamp-hours. What is an amp-hour, and what does it mean? ([Hint](http://www.allaboutcircuits.com/vol_1/chpt_11/3.html).)

Q19. When batteries are placed in series, do we just add the voltages and currents up, or do we have to do a more complex calculation?

    A. For example, if you had two, 3V batteries that were rated at 100mA, what would kind of voltage and current could we get from them if we put them in series?

    B. If we put the same two batteries in parallel, what would we get?


Q20. Matt has four AA batteries rated at 1.2V and 2100mAh each. If he puts all four in series, what will be the electrical potential of his batteries? How many milliamp-hours of current could they supply?

Q21. Matt decides to buy one of those fancy cell-phone rechargers, which is really just a lithium-polymer battery; it is rated at 3.7V and 10,000mAh. The Arduino typically draws 25mAh of current while running.

    A. How long could Matt run his Arduino using the four AA batteries arranged in series?

    B. How long could Matt run the same Arduino using the fancy cell phone recharger battery pack?

Q22. Matt has a car battery, which is rated at 12V and 70 amp-hours.

      A. Matt wants to build a voltage divider that will let him power is 5V Arduino off of the car battery. What resistors should he use in the voltage divider? Use standard value resistors (you may need to look these up.) Get close; anything between 4.85 and 5.5V will be fine. (Hint: the values are between 100 and 200.)

      B. Matt wants at least 30mA of power available to him, or the Arduino won't run. How much current can he expect from the circuit you have built? Is it enough?

      C. How many watts of power dissipation can we expect through your circuit? (Do this calculation on a circuit that is *equivalent* to your voltage divider.)

      D. Resistors typically come in .125W, .5W, 1W, and greater power dissipation values. That is, a .125W resistor can dissipate a maximum of 1/8W before the blue smoke gets out. What is the minimum power rating you need for this circuit?

      E. How much does a typical 1/8W resistor cost, and how much does a typical 1W resistor (of the same size) cost?

      F. Given the current being drawn, how long can Matt run the Arduino on the car battery?

Q23. You realize that, using the car battery, you could power your data logger without needing to plug it in! Assuming it turns on twice an hour, and it turns on for one second every time it turns on, for how long could you power your sensor using a car battery? (You know how long the Arduino will last on a car battery from the previous question; now, you have to figure out how much time it will be on if it is only on for a few second per day...)

## Thinking Forward: Solar

You decided to go solar. So, knowing how to use a soldering iron, you decide to make your own solar panels.

You [hit Amazon, and buy some](http://www.amazon.com/Prime-Solar-Cell-Tabbing-Diode/dp/B004IG5A3E/ref=sr_1_5?ie=UTF8&qid=1425133220&sr=8-5&keywords=solar+panels+diy).

Q24. If you put all 40 in series, how many volts could you, in theory, get out of your solar array?

Q25. If you put all 40 in parallel, how many volts would  you get?

Q26. What about amps, in both cases?

Q27. You want a panel that can supply **at least** 18 volts. Using *no more than* 40 individual solar cells, how could you arrange them (in terms of a parallel/series combination) to maximize your current? How many watts would you get from this panel? (Use diagrams as necessary to help me understand your answer.)

Q28. Matt needs his coffee. He buys a [DC hot water heater](http://www.amazon.com/Roadpro-12V-Hot-Pot-oz/dp/B000667QL4/ref=sr_1_6?ie=UTF8&qid=1425133500&sr=8-6&keywords=dc+water+heater). Could he power it off of your solar panel? Why or why not? If not, how many of your panels (fully assembled) would be needed to supply enough wattage to run the hot water heater? (Draw the configuration that would work.)

## Submission

Bring hard copy of your work on Tuesday after break. If you did work digitally, print it. (For example, please print your Fritzing diagram.)

This allows you to work digitally or by hand on paper. Please make sure that, where appropriate, you show your work. Be neat(ish), so I can read your answers and follow what you are doing.
