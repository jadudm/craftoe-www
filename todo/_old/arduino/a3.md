---
---

## Download Software

You should download the [Arduino IDE and ArduBlocks]({{site.base}}/docs/software.html) for Thursday. Give a shout if you have any difficulties. Also, TAs in the evening lab hours can be of assistance in this activity!

## Reading

For Thursday, you have two pieces of reading.

* [Resistors]({{site.data.urls.pdfs.eocs.resistors}})

    This is the first in a long series of component-oriented readings. We will be studying the components of electronics, discussing the role they play in circuits, and connecting them to our underlying theory. These readings will impact many of our learning outcomes over the course of the term.

* [Studying Programming: Chapter 2]({{site.data.urls.pdfs.sp.ch2}})

    We will begin exploring programming on the Arduino as a class, and we'll also be talking about what it means to engage in the practice of programming. Modern electronics is tied intimately to programming, and we'll experience some of both. Again, this is core to our learning objectives for the term.
    
## Response

There will be a short quiz at the start of class on Thursday over this material.

## Learning Objectives

* {{site.data.lo.comp12}}
* {{site.data.lo.comp21}}