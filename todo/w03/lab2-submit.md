---
---

The purpose of Lab 2 was to explore two concepts:

1. How voltage, resistance, and current are related, and

2. How resistance behaves when added to a circuit in series or in parallel.

Your writeup, in this case, should have two parts, and your goal is to your data to answer two questions.

## Describe: Voltage, Current, and Resistance

We learned Ohm's Law through our readings and discussions (V=IR). In Lab 2, you built some very simple circuits (in most cases, a single resistor!) where you chose the voltage and resistance and then measured the current in the circuit.

Describe, briefly, your experimental method and the data you collected. Then, discuss how well (or poorly) you believe your findings match with the theoretical prediction of Ohm's Law. You might do this with words, or perhaps in combination with graphical evidence from your data collection and calculations.

## Describe: Resistance in Series and Parallel

We discussed how resistors add their resistance in series, and how they sum their reciprocals in parallel. (By this, I mean 1/R<sub>total</sub> = 1/R<sub>1</sub> + 1/R<sub>2</sub>, and so on.)

Describe, briefly, your experimental method and the data you collected. Then, discuss how well (or poorly) you believe your findings match with the prediction of the mathematical summation of the resistances you arranged in series or parallel. Again, as with the previous investigation, do your findings support, or challenge, the theory that you are told is correct?

## Hypothesize

In conclusion, put forward a hypothesis. From what you have observed, do you believe you could use multiple resistors in a circuit, and still get the same kinds of results as you did in the first half of the lab?

Propose one, possibly two experiments that would allow you to explore the relationship between voltage, resistance, and current with respect to resistors in both series and parallel. In putting forward the experiments, describe (possibly even sketch) what you expect the results of these experiments would be.

## Submission

For graphs, you are encouraged to either neatly draw them by hand (and snap a photo to include in your report), or you may explore the use of Excel. If either of these approaches present difficulty, please do contact me, and I'll help out.

Upload your report to Moodle.
