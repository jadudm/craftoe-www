---
---

Lab 3 is described in the book (and, fittingly, labeled "Lab 3"). It involves further investigation of the relationship between voltage, resistance, and current.

We will use both days this week to explore the material of Lab 3. This is also in anticipation of questions that might consume some of our time in class. Therefore, we will plan this week on having enough time for both Q&A as well as doing our physical explorations of electronic circuits involving resistors, LEDs, and switches.
