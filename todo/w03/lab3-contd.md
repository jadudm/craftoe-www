---
---

We will use both days this week to explore the material of Lab 3.

You should read "Chapter 2," which followed the lab we just did in class. It begins to connect the theory to the measurements you made.

There are several videos linked; if you do not watch them for Thursday, we will be watching them over the weekend. They demonstrate/walk through the mathematics of dealing with voltage drops in a circuit.

I will be unsurprised if there are questions about this in class as well.


NOTE: One of your colleagues (OG) will be working with me over the coming weeks to help make sure the numbering of the chapters is more sane (among other things). Feel free to report bugs/confusion as you read.
