---
---

You and your partner have been given three resistors. For this lab, you are investigating how resistors behave when arranged in different ways. Specifically, how they behave when arranged in *series* and in *parallel*. You will get more practice building circuits, using the multimeter, and (ultimately) reporting your results in writing and using Fritzing (for diagramming).

## What Do The Colors Mean?

Google the sequence of colors in the room, starting at the coffee/tea station, and see what you discover.

## Three Resistors

If you are a fan of permutations, you know there are 2P3 (or six) possible ways to arrange these resistors when you choose two at a time. ([Combinations and permutations](http://www.mathsisfun.com/combinatorics/combinations-permutations.html) are fun; take a look sometime later.)

Using your meter, explore the following. Remember to record your notes, neatly, in your notebook. Sketch the circuits you are building as you go (in your notebook, again) so you can later translate them to Fritzing diagrams.

### In Series

Assuming your resistors are named Alice, Bob, and Clarence, you can arrange them as follows:

    Alice - Bob - Clarence

or, if you like,

    Bob - Alice - Clarence

or... well, you get the idea. There are many ways to arrange them.

Build a collection of circuits with two and three resistors, measuring the resistance in every case. (This means you **need the meter in the Ω mode**.) You should build a table in your notebook where you note which resistor(s) you used in your circuit, and what you measured in terms of resistance. When you're done, see if you can come up with a mathematical relationship that lets you predict, for any combination of resistors in series, what the resistance will be.

### In Parallel

Take the same resistors, and now arrange them in various combinations in parallel. For example, you might do something like this:

       --- Alice ---
       |            |
    ---              ---
       |            |
       ---- Bob ----

Then, you could swap Clarence in, or perhaps you could put all three in parallel. Again, using your multimeter, measure the resistance of these circuits, and see if you can ascertain what is going on. Specifically, you are looking for a mathematical relationship that would let you predict (correctly) the resistance of any two, three, or more resistors arranged in parallel.

**Hint**: Start with two, and think about the inverse of what you found in your previous experience involving resistors in series.

Again, make note of everything you do in your notebook.

### In Series and Parallel

Finally, build some circuits like this:

                --- Alice ---
                |            |
    Clarence ---              ---
                |            |
                ---- Bob ----

You should be able to figure out what the resistance is of each part of the circuit (for example, the resistance of Clarence vs. the resistance of Alice and Bob in parallel), and then measure the resistance of the full circuit. Can you, using what you discovered in the previous two parts of the laboratory, figure out what is going on in circuits of this form?

## Reflection / Submission

You should discuss these questions with your partner, and answer them before sitting down to write your report. You should each submit your own independent answers, but you are welcome to discuss in advance and make sure you're both on the same page and have taken away good learning from the lab.

* **Prompt**: Investigating Resistors ([docx](http://goo.gl/dQmS0c), [Google Docs](http://goo.gl/68yjBA))

* **Submission**: [Moodle]()