---
---

Before we get started, we're going to "take stock." Specifically, we learned quite a few things during our first lab, and we'll take a minute, as a group, to share out that learning and capture it in our notebooks.
