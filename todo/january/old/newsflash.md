---
---

From this point forward, all work will be submitted via Moodle. 

Assignments will have a Moodle submission link at the bottom, and it should be obvious. If you have questions, ask, but my belief is that it will be clear.

Apologies for any confusion.
