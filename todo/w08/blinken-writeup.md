---
---

Your blinken writeup is due today. It is a short document that captures what you did on Thursday, Feb 18th. It was originally described on the smartboard, and is re-created here for clarity.


## Writeup

Use Word/Libre Office to put together a short report on your first programming/blinkenlights experience.

1. Explain, as if writing for a peer, how you designed your circuit for a traffic light with 3 LEDs. Your description should be in relation to both a schematic (that you drew in SchemeIt) as well as a photograph of your actual circuit.

2. Include your code -- a screen capture (by pressing Print Screen on Windows) is a necessary addition. Describe what your code does, block-by-block.

3. Finally, reflect on the first two chapters of *Studying Programming* and how your experience writing this first program for the Arduino does, or does not, reflect some of what you read there.

## Submission

Moodle.
