---
---

On Thursday, we're going to begin a dive into *voltage dividers*. We'll begin by investigating the *light dependent resistor*, and then integrating it into a circuit that we read with our Arduino.

As has been our habit in this course, we will do this by investigating the phenomenon, and then developing and applying the electronic theory (the math part).

Although it is not explicitly stated, you should be making notes in your notebook, capturing appropriate images/photos along the way, and generally preparing to be able to report on and document your exploration. That should not be surprising or new at this point.

## Before the Lab

Watch. Listen.

This one has some funky, funky grooves:

<div class="text-center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/1hqn3Db9xUA" frameborder="0" allowfullscreen></iframe>
</div>


## Characterizing the LDR

Next, with your trusty alligator clips and your multimeter, see if you can make sense of the LDR. It is a resistor... but, what kind of resistance do you read in the room? By the window? With a laser pointer? 

## In a Circuit

Next, build it into a circuit. Given the maximum resistance that you measured, choose an appropriate resistor to put in series with the LDR. Build a circuit that goes from...

1. Your power supply to

2. Your resistor to

3. Your LDR to

4. Ground (or, if you prefer, the negative side of the supply)

Now, use your multimeter to measure the *electrical potential* (that means *voltage*, remember) between ground and the point between the resistor and LDR. How does that voltage change when it is bright? Dark? 

## With an Arduino

Finally, it is time to bring your Arduino into the mix.

Disconnect your power supply, and instead use the 5V and GND connections on the Arduino for powering your circuit. Instead of using your voltmeter at the middle point, instead run a jumper from the point between the resistors to Analog Pin 5 (A5).

You want to write a program that does the following, in a loop:

1. Takes an analog reading from A5.

1. Prints that value.

1. Delays for one second.

That number will be a value between 0 and 1023. Divide it by 1023, and then multiply by 5.0 to get the number of *volts* that the Arduino is reading.

<div class="text-center">
  <iframe width="853" height="480" src="https://www.youtube.com/embed/D9wX5TMMm7A?rel=0" frameborder="0" allowfullscreen></iframe>
</div>

Walk around with your Frankenstein laptop/sensor configuration to take voltage readings in at least three different locations around the building. Record your findings and be prepared to share them out with the rest of the class.

## Submission (But Not)

We will be following up with this data, and be exploring other sensors. Make sure your notes are thorough, as some aspects of this work will be integrated into a later report. There is no explicit report on this right now. It will show up, though, as we explore additional sensors and as we put our theory of resistors-in-series to work again.

## Completely Unrelated

And, finally, completely unrelated, but I liked it. You are not required to watch it at all...

<div class="text-center">
  <iframe src="https://embed-ssl.ted.com/talks/james_veitch_this_is_what_happens_when_you_reply_to_spam_email.html" width="854" height="480" frameborder="0" scrolling="no" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
</div>

(This is the first non-work thing I've looked at in weeks. Hence, I'm sharing.)
