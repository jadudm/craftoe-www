---
---

## Prep

You may find it valuable to revisit [voltage drops]({{site.base}}/todo/w04/voltagedrops.html).

## The Context

On Thursday, we began with an exploration of voltage dividers using an LDR, which is a kind of variable resistor. What follows are a series of questions that require reasoning, calculation, and possibly some more investigation using your Arduino doing an "analog read" and/or the multimeter.

You have two circuit configurations, Alphonso and Bertrand. Both have an LDR with a 0K to 50K range, and a 10K resistor in series. Alphonso looks like this:


    +5 ----/\/\/\/\/\------o------/\/\ LDR /\/\----- GND
              10K          |
                         Analog


Bertrand is backwards; pay attention to the voltage source and ground:


    GND ----/\/\/\/\/\------o------/\/\ LDR /\/\----- +5V
                10K         |
                          Analog


## The Questions

1. What is the range of readings we can expect on the Arduino? 

1. For Alphonso, when it gets lighter, what kind of readings (high/low) will we get on the Arduino?

1. For Alphonso, when it gets darker, what kind of readings will we get on the Arduino?

1. What voltage does a reading on 512 on the Arduino correspond to, roughly?

1. What voltage does 0 correspond to?

1. What voltage does 1023 correspond to?

1. Looking at Alphonso, we measure a 2V drop across the 10K resistor. How many volts must we be dropping across the LDR?

1. What voltage will we read on our analog input pin if there is a 2V drop across the 10K resistor?

1. (Alphonso) The LDR is under medium light conditions, and has a resistance of 22K. How many volts will we read on the analog read pin? (This is a circuit with multiple resistors; set up your table and calculate V=IR, then the drop for each resistor, and so on. The Eris videos from before break might help remind you, or Chapter 5 from All About Circuits.)

1. (Alphonso) The LDR is under bright light conditions, and has a resistance of 100Ω. How many volts will we read on the analog read pin?

1. (Alphonso) You cover the LDR with your thumb; it has a resistance of 48K. How many volts will we read on the analog read pin?

1. In the previous three questions, you calculated a voltage. What number would the Arduino report on its analog read pin in each of the previous three measurements?

1. For Bertrand, when it gets lighter, what kind of readings (high/low) will we get on the Arduino?

1. For Bertrand, when it gets darker, what kind of readings will we get on the Arduino?

1. (Bertrand) The LDR is under medium light conditions, and has a resistance of 22K. How many volts will we read on the analog read pin? (This is a circuit with multiple resistors; set up your table and calculate V=IR, then the drop for each resistor, and so on. The Eris videos from before break might help remind you, or Chapter 5 from All About Circuits.)

1. (Bertrand) The LDR is under bright light conditions, and has a resistance of 100Ω. How many volts will we read on the analog read pin?

1. (Bertrand) You cover the LDR with your thumb; it has a resistance of 48K. How many volts will we read on the analog read pin?

1. Look at the pair of calculations you carried out using Alphonso and Bertrand, when the LDR had a resistance of 22K. What is true/interesting about the two values you calculated?

1. Look at the pair of calculations you carried out using Alphonso and Bertrand, when the LDR had a resistance of 100Ω. What is true/interesting about the two values you calculated?

1. Look at the pair of calculations you carried out using Alphonso and Bertrand, when the LDR had a resistance of 48K. What is true/interesting about the two values you calculated?

1. Matt wants to get readings between 0 and 512 for low voltages, meaning bright conditions. Should he use a circuit like Alphonso or Bertrand?

1. Matt wants to get good, sensitive readings under bright light conditions. Should he keep the 10K resistor, or should he change it? Specifically, would you use a 100K resistor, or a 1K resistor? Defend your answer with either experimental evidence and/or calculations.

1. Matt wants to get good, sensitive readings under dark conditions. Should he use Alphonso or Bertrand as a configuration?

1. Should he keep the 10K resistor, or should he change it? Specifically, would you use a 100K resistor, or a 1K resistor? Defend your answer with either experimental evidence and/or calculations.

## Working the Problems

You are encouraged to work with a partner on these questions, to ask questions, and to pull out your meter and/or Arduino and experimentally verify anything that you are calculating. The larger goal is to develop intuitions and understandings about how voltage dividers work. These questions have to do that through a variety of methods, and you might develop questions and experiments of your own to verify and validate your own understanding.

## Submission

Write up your experiments, observations, and calculations and submit them to Matt or an Amazing TA. Hand-written is fine, but you should 1) make sure it is very neat, 2) include the names of the colleagues you worked with, 3) have your own copy, and 4) be confident that you can recreate the work that is submitted. By that, I mean that you should be able to do the work that you submit on your own (or mostly on your own) at a later point. 

In a nutshell: submitting work you do not understand does not help.

Electronics TAs are available Sunday, Monday, Tuesday, and Wednesday evenings in the from 7-9PM.
