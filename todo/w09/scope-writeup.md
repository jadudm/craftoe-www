---
---
From [The Exploring Das Blinkenlight](../w07/blinkenscope.html) lab with the scope.

Your writeup should have four sections:

* INTRODUCTION

    In the introduction, you should describe the phenomenon you are studying and the tools you are using to do your exploration. Make sure you describe these things in enough detail (using diagrams, images, and words) so that your reader (whom you can assume to be a sophomore/junior in college, but not necessarily a student of CS or TAD) could understand what you're writing about.
    
* METHOD
    
    In the method section, you describe the exploration you undertook, and how you went about it. This is where the code that drove your experiment would be discussed, the circuit itself, and where you were taking measurements of that circuit (as well as how you were obtaining your data).
    
* RESULTS

    In the results section, you discuss the results of your experiment. This is where you might include one or more of your images from the scope. You could also report the data in one or more tables, a chart, or any other method that you believe clearly and easily represents the data that you've captured. Including every scope image is probably not necessary. 
    
* DISCUSSION

    In the discussion section, you make sense of your data. You discuss with the reader what you found, and how the reader should make sense of the findings. You may find yourself needing to do some research on the Internet to help make sense of the data. 
    
    First, you should help them understand what the data is saying, without significant interpretation. For example, if all of your readings are exactly precise, then you can say that. If all of your readings are slightly higher than the value you programmed, you say that, too. Your job is to first help the reader 1) understand what you measured and 2) what it seems to be saying.
    
    Second, you then need to try and explain that data. For example, if you found that a 1ms delay measured as 1.04ms, you clearly have a discrepancy between your code and the observed reality with your scope. You should do your best to come up with one or more reasonable hypotheses ("educated guesses") that might explain why you observed what you did. This can be informed by research on the Internet, discussion with colleagues (remember to cite who you talk with---give credit where credit is due!), and so on. In short, your job is to try and figure out *why you saw what you saw*, and explain that, as best as you can, to your readers.
    
# Submission

Moodle.
