---
title: Software
resource: local
layout: default
---

There are a lot of different tools we'll be using this term. People have complained about this in the past. I'm afraid there's nothing I can really do about that... we need tools to do what we do!

## Fritzing, Arduino, and Ardublocks

There are several programs we will make use of this term.

* [Fritzing][fritzing]. We use this to draw circuits and diagrams.

* [Arduino IDE][arduino]. We use this to program our Arduino.

* [ArduBlocks][ardublocks]. This is an add-on for the Arduino IDE that gives us a drag-and-drop interface to programming the Arduino.

Please note: The ArduBlocks interface is a version that has been modified for use in CRAFToE. It is open source software that, in its original form, can be found here. The version you are using is modified for specific use in our class. (The changes are entirely cosmetic.)

After downloading the Ardublocks JAR file (ardublocks-all.jar), you will need to [follow the installation instructions that you can find online to properly install it into the Arduino IDE][install].

[fritzing]: http://fritzing.org/download/
[arduino]: http://arduino.cc/en/Main/Software
[ardublocks]: {{site.base}}/resources/ardublock-all.jar
[install]: http://blog.ardublock.com/2011/10/25/install-ardublock-arduino-ide/


## Cel.ly

Celly will be used for alerts and updates. Students have repeatedly asked for ways they can be notified about changes to the website and other course announcements via *anything but* email. Celly provides me with a way to post messages that **you can choose** to have received via SMS, email, or via an app you can install (Android or IOS).

You can create an account using Twitter/Facebook/Google credentials, but **your profile must use your berea.edu address**. This is how I am making sure only Berea students can join into our "cells." You will want to join the cell <b><a href="{{site.data.celly.url}}">{{site.data.celly.name}}</a></b> after you create your account. 

<div class="text-center">
  <iframe width="640" height="360" 
          src="//www.youtube.com/embed/CHAU4qjD-i4?rel=0" 
          frameborder="0" 
          allowfullscreen
          ></iframe>
<br/>
</div>

## Q.berea

Q.berea is a free/open question and answer package living at [q.berea.mobi](http://q.berea.mobi/). Please create an account here. I installed and am hosting this service.

**NOTE**: Do not use a completely garbage password, but please do not reuse your Berea password, either. I say this because I cannot provide you with a secure connection (HTTPS) for Q.berea, and therefore your password is being transmitted in the clear. For that reason, don't reuse something you care about.

