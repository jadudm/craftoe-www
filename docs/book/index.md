---
---

# Book: The Craft of Electronics

This semester, I will be assembling what used to be material posted to the website into a book. I will be updating it all term --- essentially writing it as we go. (I have written this material multiple times, in multiple ways, on the course website in previous offerings of the course... so, consider that I have written multiple drafts over the past several years.)

You will be responsible for readings in this book, as well as other resources that it links to (textual, audio, and video). You will need to download the most recent version of the text throughout the term; if that is annoying, I'm sorry.

## Revision History

The most recent version is always linked at the top.

* [Version 20160126 - Added Voltage Drop chapter](jadudm-craftoe-20160126.pdf)

    Added the voltage drop chapter that follows lab three.  It is unclear whether the LED voltage drop chapter is the next one we should have in, or perhaps a simple LED/switch chapter. However, knowing how to calculate a voltage drop is part of protecting LEDs with a resistor, so... it makes sense, conceptually, even if it isn't very exciting.

* [Version 20160125 - Added Lab 3](jadudm-craftoe-20160125.pdf)

    Got tired. Stopped for the night. Spent a lot of time in the past two days bringing this together, but now need to finish the LED bit. Will aim for Tuesday morning.

* [Version 20160117 - Added Lab 2](jadudm-craftoe-20160117.pdf)

    This version adds lab 2. Lab 2 was a bit late being added, which is unfortunate, but will happen from
    time to time. Lab 2 is a fairly straight-forward experiment, and currently comes with no pre-lab reading
    (beyond reading the lab itself).

* [Version 20160113 - Improved Lab 1](jadudm-craftoe-20160113.pdf)

    This version improves the Lab 1 chapter slightly, adding an experiment involving
    the destruction of resistors, and improving the description of the writeup.

* [Version 20160111 - Chapter 1, Lab 1](jadudm-craftoe-20160111.pdf)

    This was the first release. It was what it was.
