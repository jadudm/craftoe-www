---
title: Electricity and Electronics Syllabus
resource: local
layout: default
gavel: "<i class='fa fa-fixed fa-gavel'></i>"
---

{% include twocol label="Course" value=site.data.course.name %}
{% include twocol label="Number" value=site.data.course.number %}
{% include twocol label="Instructor" value="Matt Jadud" %}
{% include twocol label="Office" value="Danforth Tech 102B" %}
{% include twocol label="Phone" value=site.data.course.phone %}
{% include twocol label="Email" value="jadudm" %}

### Office Hours
{% include officehours.md %}

{% callout info %}
This syllabus may be amended, edited, or otherwise updated if deemed necessary by the instructor.
{% endcallout %}

## Why should you care about {{site.number}}?

What is electricity, and how can I safely work with it? How do I design and build electrical systems? How do I sense and automate the world around me using low-cost, low-power computing systems? Ten years ago, answering these questions was the provenance of electrical engineers; today, it is possible for anyone to develop the skills and tools to design, build, and program sensing and automation systems to monitor and control the world around them. 

## How will this course help you succeed?

As a student of technology and computing, understanding digital electronic systems is essential to your work. As someone who enjoys making things, debugging them when they don't work, and solving interesting problems, you will have the opportunity to develop and practice those life-long skills hands-on in this course. By the end of the course, you will have been launched down a path that lets you wear many hats, and explore the following questions for the rest of your life. As a...


* **MAKER**: How do I use the tools of the global electronics maker community to create, communicate, and collaborate on electrical systems?

* **DESIGNER**: What knowledge of electronic components (and how they come together) do I need to design circuits to sense and automate the world around me?

* **COMMUNICATOR**: How do I interpret and create the many textual and visual representations of electronic circuits, and use that information to build circuits that work?

* **PROGRAMMER**: How can I prepare myself to program systems that sense and automate the world around me, even if I've never programmed a computer before?

* **THEOR**: What are the connections between the theories of electricity and electronics to our daily lives?

* **INVENTOR**: What questions can I ask about myself and the world around me, now and in the future, that sensing and automation systems can help me answer?

## Where can I look for information?

Anywhere! We will be using a combination of resources this term. We will use the course text, but will also rely heavily on resources found online (textual and video). An important part of this course is to help you learn to leverage many diverse resources and extract from them the essential information you need to solve the challenge you are investigating at the moment—this is an essential skill that we will practice throughout the term.


## Statements Regarding Accommodations

Berea College will provide reasonable accommodations for all persons with disabilities so that learning experiences are accessible. If you experience physical or academic barriers based on disability, please see Lisa Ladanyi (Disability & Accessibility Services, 110 Lincoln Hall, 859-985-3327, lisa.ladanyi@berea.edu) to discuss options. Students must provide their instructor(s) with an accommodation letter before any accommodations can be provided. Accommodations cannot be provided retroactively. Please meet with your instructor(s) in a confidential environment to discuss arrangements for these accommodations.

Under Title IX of the Education Amendments of 1972, pregnant and parenting students may be afforded certain accommodations regarding their educational experience.  If you believe that pregnancy or pregnancy-related conditions are likely to impact your participation in this course, please contact Berea’s Title IX Coordinator, Katie Basham, to discuss appropriate accommodations.  She may be reached at Katherine_basham@berea.edu or 859.985.3606.


## How will you succeed in this course?

* **PARTICIPATE**. You are expected to be present regularly (meaning, always), and actively engage in the reading, conversation, and practical work of the course. More than just "showing up," however, active participation means that you are questioning, making connections, and thinking about where the material you are learning fits in with what you already know, as well as what you are learning in other courses. 

* **COMMUNICATE**. I strive to answer questions promptly, and have set up a forum where you can ask questions of myself, the TAs, and the rest of the class. **The forum should be your preferred mode of asking questions about the content of the course**. Learning to interact professionally in such forums is a good idea, as they are a common tool for many maker communities online today.

* **ASPIRE**. You aren't going to learn by doing the bare minimum. Dive in, have fun, and do your best to do your best. By striving for excellence, you push yourself to master new ideas and get the most from your learning. Instead of reading just one source, read three. Instead of starting your work the night before it is due, start the day it is assigned. Instead of getting frustrated and spending hours banging your head against the desk, ask a question early... or, help answer someone else's question!

## How will you and I evaluate your progress?

We will have a shared document that captures your achievement of the [learning objectives](learningobjectives.html) for this course. You should be roughly 1/2 way through the learning objectives at the midterm, and you should have completed all of them by the end of the term. If your completion rate is too low (meaning, you're failing to finish assignments, submit revisions, or generally doing work of a low quality), we will meet to discuss your work and your strategy for getting back on track towards completion and excellence.

More details follow (below) regarding how your efforts will be measured and assessed in this course.

## Course Learning Goals 

If this was the *only* course in electricity and electronics you take at Berea College, and you never touch or study the subject again while you are a student, what questions do I hope you will be able to answer 10 years from now?

1. <a name="safe"></a>**How can I be safe in everything I do?** <br/>

1. <a name="comms"></a>**How do I communicate, with clarity, the results of my efforts?** <br/>

1. <a name="design"></a>**How can I design electrical systems to sense and interact with the world?** <br/>

1. <a name="build"></a>**How do I build and program those systems?** <br/>

Course goals provide the "big picture" overview; our learning objectives are the measurable steps towards each of these goals.

# Learning Objectives

Learning objectives are the assessable points of learning in the course. The number of these learning objectives you complete is what determines your final grade in this course.

These objectives are subject to change. In particular, the "jury is still out" on objectives marked with a {{page.gavel}}.

1. [Craft: Fabrication and Safety](#craft)

1. [Theory of Electronics](#theory)

1. [Components and Build](#build)

1. [Learning and Justice](#world)

## Craft: Fabrication and Safety

<a name="craft"></a>

This material will be covered over the course of the entire term, and will be assessed through quizzes, exams, structured reporting, and direct observation.

### Level 1

1. Demonstrate understanding of basic breadboarding practice, including the use of wire strippers, crimpers, and related tools.

1. Use Fritzing for breadboard diagrams of basic circuits.

1. Identify basic components in schematic form.

### Level 2

1. Demonstrate (through written material and practical demonstration) safe use of a soldering iron.

1. Use Fritzing for schematic capture.

1. Identify basic components in PCB form.

1. Build basic circuits from a schematic.

1. Use a multimeter for basic diagnostics (continuity, voltage, current).

1. Discuss and demonstrate good multimeter safety.

### Level 3

1. Lay out single-sided PCBs in Fritzing and cut on a PCB mill.

1. Use an oscilloscope for basic diagnostics of oscillating circuits. {{page.gavel}}

1. Reverse-engineer circuit schematics from a PCB.

1. Interpret datasheets for basic data (eg. maximum safe voltage and current limits).

## Theory of Electronics

<a name="theory"></a>

### Level 1

1. Use V=IR to calculate whether circuits are safe for components and/or people.

1. Use P=IV to calculate power consumed by a device.

1. Read and interpret basic component values (resistors, capacitors).

1. Know safe limits for voltage and current (for people).

### Level 2

1. Use V=IR and P=IV together to solve for unknowns.

1. Demonstrate understanding of series/parallel reductions for resistance, capacitance, and voltage.

1. Know basic truth tables.

1. Demonstrate parallel/series switch networks and relate them to logic systems.

1. Build and demonstrate working pull-up and pull-down resistor configurations.

### Level 3

1. Build and calculate appropriate voltage dividers for analog sensor use.

1. Understand basic control theories (closed-loop, open-loop, PID). {{page.gavel}}

1. Demonstrate basic statistical knowledge as applied to measurements — mean, median, mode, error, etc.

1. Demonstrate calibration or mapping between values.

## Components and Build

<a name="build"></a>

This category of learning outcomes is subject to a fair bit of revision. Specifically, some of these outcomes are about programming, and it might be nice to see those outcomes in their own category... even if it is only to clean this up a bit.

### Level 1

1. Demonstrate knowledge of basic tools (soldering iron, solder sucker, wire stripper).

1. Demonstrate knowledge of passive components:
    * Resistors
    * Capacitors
    * Diodes

1. Build circuits from multiple kinds of instructions (schematic, text) using passive components.

### Level 2

1. Demonstrate the ability to actuate (on, off) components using code.

1. Demonstrate ability to read the state of binary inputs (eg. buttons) and use conditionals in code.

1. Demonstrate knowledge of switching components:
    * Transistors
    * Solid-state relays
    * Mechanical relays
    * Buttons/DIPs/Switches

### Level 3

1. Demonstrate knowledge of analog input and output

1. Demonstrate the ability to develop subroutines for code reuse. {{page.gavel}}

1. Demonstrate knowledge and use of basic actuators:
    * Servos
    * DC Motors
    * Steppers
    * Solenoids

1. Demonstrate knowledge and use of basic analog sensors:
    * Temperature
    * Light
    * Force
    * Moisture

1. Demonstrate ability to compute over values (write expressions) in code.

## Learning and Justice {{page.gavel}}

<a name="world"></a>

An important theme of many of the assignments in the course is one of building a foundation you can use to continue the exploration of electronics on your own. Because of this, we will (at times) be explicit in our discussion of learning, our reflection on our own learning, and the societal pressures that might stand in the way of our success as learners.

In other words, some of our reading discussion will center on questions of race, sex, gender, and socioeconomic status, and the challenges we might encounter throughout our lives as students of electronics.

### Level 1

1. Read and reflect (in writing) meaningfully on videos and blog-style content on the topic of learning.

### Level 2

1. Discuss, through semi-structured interview (or structured journal) on topics of success and challenge regarding learning in electricity and electronics.

1. Discuss respectfully, in small-group contexts, on topics of race, gender, and socioeconomic status in the context of learning in electronics and STEM-related disciplines.

1. Practice intense listening.
 
### Level 3

1. Listen, intensely and with empathy, to peers in small- and large-group settings.

1. Meta-reflect, in writing, on personal strengths and weaknesses in learning, by reflecting back over previous writings and documents regarding the practice and learning of electronics.

1. Identify directions for further growth and study in electricity and electronics.

{% comment %}
## Habits of Practice

These are not learning objectives *per se*, but instead represent practices that you are expected to engage in all term.

1. Engage in question asking and answering in {{site.data.urls.qberea}}, our course discussion tool.

1. Get together with your learning team(s) at least once per week outside of class. {{page.gavel}}

1. Journal regarding your learning successes and challenges. (This might also be thought of as a *learning trajectory log*, where you reflect on what objectives you are making progress on, and so on.)

These will likely be incorporated explicitly in assignments.
{% endcomment %}

# Assessment

{% callout danger %}
These criteria will be evaluated by the faculty and TAs, and may be subject to change.
{% endcallout %}

Completion of the learning objectives accounts for 90% of your final assessment in {{site.name}}. Each objective can be completed at one of four levels:

* **Excellent** <br/>
    
    To earn an **A** in {{site.name}} on an objective, you must do it with excellence. This means you have completed all of the background work, answered questions thoroughly, and demonstrated depth of thought and consideration as part of your work. Excellent work is submitted on-time.

* **Good** <br/> 
    
    To earn a **B** in {{site.name}}, you have submitted your work on time, and done it well. You might have a small mistake or conceptual error (that can be fixed), and it might not demonstrate the depth of consideration and reflection that truly excellent work brings to the table. Most work done with intention and care is *good*.
    
* **Average** <br/>

    Average work (**C**) is late, incomplete in some way, and/or incorrect in one or more ways. The majority of the work is of high quality, but some aspect of it is done poorly or incorrectly. There is an attempt made at reflection, but no real depth of thought or consideration. Students rushing their work or otherwise failing to manage their time well often submit work that demonstrates *average* quality and effort.
    
* **Poor and Failing** <br/>

    Anything below average is essentially poor (**D**) or failing (**F**).



Participation accounts for 10% of your final assessment. It is judged by your instructor (possibly in conjunction with your peers); your participation is a reflection of the professionalism and commitment you bring to your learning and the learning of those around you.

# Revision

Work must be turned in on time to be eligible for revision.

Work turned in late can never achieve an *E*xcellent rating.


## Grade Distribution

<div class = "">
  <div style = "width: 50%; margin: 0 auto;">
  <div class = "row">
    <div class = "col-md-6">
      <b>Category</b>
    </div>
    <div class = "col-md-4">
      <b>Weight</b>
    </div>
  </div>
  <div class = "col-md-10">
    <hr>
  </div>
  <div class = "row">
    <div class = "col-md-6">
      Quizzes and Exams
    </div>
    <div class = "col-md-4">
      30%
    </div>
  </div>

  <div class = "row">
    <div class = "col-md-6">
      Individual Lab Reports
    </div>
    <div class = "col-md-4">
      30%
    </div>
  </div>
  
  <div class = "row">
    <div class = "col-md-6">
      Developmental Assignments
    </div>
    <div class = "col-md-4">
      25%
    </div>
  </div>
  
  <div class = "row">
    <div class = "col-md-6">
      Participation and Professionalism
    </div>
    <div class = "col-md-4">
      10%
    </div>
  </div>
  
  <div class = "row">
    <div class = "col-md-6">
      Peer Evaluation
    </div>
    <div class = "col-md-4">
      5%
    </div>
  </div>
</div>
</div>

## Expectations and Attendance

To achieve the learning outcomes in this course, you may need to alter some of your prior behaviors.

<style type = "text/css">
tr.line td{
border-bottom: 1px solid #B4B5B0;
}
</style>

<table width = '80%' align="center" >
  <tr style="background: #eee;" class = "line">
    <td style="border-bottom-style: solid;"> Traditional Classroom <br/> <small>Students shift from...</small></td>
    <td width="5%" style="padding: 1em;"> </td>
    <td> Collaborative Classroom <br/> <small>to...</small></td>
  </tr>
  <tr class = "line">
    <td>Listener, observer, and note taker</td>
    <td><i class="fa fa-arrow-circle-o-right"> </i></td>
    <td>Active problem solver, contributor, and discussant</td>
  </tr>
  <tr class = "line">
    <td>Low or moderate expectations of preparation for class</td>
    <td><i class="fa fa-arrow-circle-o-right"> </i></td>
    <td>High expectations of preparation for class</td>
  </tr>

  <tr class = "line">
    <td>Private presence in the classroom with few or no risks</td>
    <td><i class="fa fa-arrow-circle-o-right"> </i></td>
    <td>Public presence with many risks</td>
  </tr>

  <tr class = "line">
    <td>Attendance dictated by personal choice</td>
    <td><i class="fa fa-arrow-circle-o-right"> </i></td>
    <td>Attendance dictated by community expectation</td>
  </tr>

  <tr class = "line">
    <td>Competition with peers</td>
    <td><i class="fa fa-arrow-circle-o-right"> </i></td>
    <td>Collaborative work with peers</td>
  </tr>

  <tr class = "line">
    <td>Responsibilities and self-definition associated with learning independently</td>
    <td><i class="fa fa-arrow-circle-o-right"> </i></td>
    <td>Responsibilities and self-definition associated with learning interdependently</td>
  </tr>

  <tr class = "line">
    <td>Seeing teachers and texts as the sole sources of authority and knowledge</td>
    <td><i class="fa fa-arrow-circle-o-right"> </i></td>
    <td>Seeing peers, self, and the community as additional and important sources of authority and knowledge</td>
  </tr>

</table>

## Class Atmosphere

I want many things for students in my classes, and I very much want you to help me achieve these goals.

* I want our laboratory to be a relaxed environment where you are **comfortable trying new things** and (sometimes) failing. By "failure" I do not mean "receiving an F," but I do mean that you try things, make mistakes, and learn from them. The last bit---learning from our mistakes---is the critical part. Neither I, nor you, nor your classmates should put down or belittle a classmate for trying.

* I want you to **look forward to {{site.short}} because it is fun**. We should be comfortable with each other---humor and laughter makes the day go faster and better. That said...

* We should **work hard, and be proud of that effort**. For me, a "fun" day is one where I've worked hard and improved myself. I have done my best to design a course that will be fun because it challenges us to work hard and do new and interesting things.

* **Respect matters**. Respect for each-other, regardless of where we are from and where we are on our life journey is of utmost importance. I have a great deal of respect for your effort as a student; I show that respect by challenging you to extend your limits, and supporting you to the best of my ability as you take risks and engage with the course throughout the semester.


## Attendance

We will be actively engaging during class time in pair programming and group discussion activities throughout the term. Attendance is part of being professional as a student; each missed class may cost you up to 2.5% off your final grade.

If you arrive particularly late, leave particularly early, or fail to participate in class, I will consider you absent.

If you miss class for reasons that are not considered legitimate, you are responsible for getting caught up. That does not mean you should write me, apologize for being absent, and then ask me what you missed. Start with a kind classmate, and see how you fare.

More than 4 unexcused absences will be grounds for failure of the course, as this represents two full weeks of learning. Too much time and effort is expended by your faculty, TAs, and your classmates (working along side you in your learning) to accept absences from the learning activities of TAD 265. At four unexcused absences, you may be asked to leave.

### Legitimate Reasons You Might Be Absent

If you legitimately cannot be present, you will find a way to communicate that to me in advance.

* If you have flu-like symptoms, email me and visit the health center. The Center for Disease Control recommends you stay home for at least 24 hours after the symptoms pass---RESTING. Without evidence from the health center of your visit, you are not excused.

* Job Interviews

* Documented Family Emergencies / Deaths

  
## Technology Policies

Much of the work in this course will require use of the computer, so these policies are designed to help you better understand how to be effective in a technology-rich environment.

* **Laptop and Software**: We will regularly make use of laptops during class, and you are expected to have them unless explicitly stated otherwise.

* **Communication**: The course website is your primary source for information about this course. Messages about the course will often be sent by email. These are all mechanisms you would likely use in a professional position in the real world (corporate websites, internal content management systems, and email)---you are, likewise, expected to use them in a responsible and professional manner in this course.

* **Backups**: All students are expected to back-up their work on a daily basis, which includes laboratories, assignments, and quizzes. The best way to do this is to store a copy of all work in a cloud service such as Dropbox, SkyDrive, Google Drive, or to use a DVD, flash drive, or some other media. Storing multiple copies of something on your laptop is not a backup. I will not be sympathetic to lost work in any way, shape, or form.

* **Mobile Devices**. Generally speaking, you do not need to be texting and Facebooking with your mobile device during class. You have things to do. Mobile phones can be powerful tools, but my experience is that they take away from collaborative learning because of the distraction from work and learning they represent. Silence them, and put them away.

## Evening Lab / Support

The Computing and Digital Crafts Lab is open Sunday through Thursday from 7:00 to 9:00 PM (except on evenings of convocations). The primary teaching assistant and several other TAs will be able to answer questions about the content in the course during consultations in their  Lab hours. You are strongly encouraged to make use of the help available in the Computing and Digital Crafts Lab, as well as in the instructors' office hours. Best results are obtained trying to solve problems before asking for help, and you should be prepared to show what you have already tried. Topics in this course build throughout the course, so you should be sure to do your best to keep up with the class, so as to not fall behind. No question to which you do not know the answer is "dumb" unless it goes unanswered because it remained unasked.


## Catalogue Description

A study of the theory and techniques necessary for electrical and electronic systems and their associated equipment. Students will learn how to identify, calculate, measure, create, and repair basic electrical and electronic systems. These skills will be applied to a selection of practical projects that will challenge the students understanding of the material and problem solving/troubleshooting abilities. Topics may include AC/DC circuits, resistance, power, various components, and use of electrical measuring instruments. 

{% callout danger %}
This syllabus is a living document. It may change.
{% endcallout %}
